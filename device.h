/* 

	Original file name: device.h
	Name			  : Communication with EEPROM. For SFS driver
	Version			  : v1.0
	Date			  : 2010-09-05
	Compiler		  : ImageCraft ICCAVR

			  (c)Copyleft 2010, Azuolas - Faustas BAGDONAS

*/

#pragma once
#ifndef __DEVICE_H
  #define __DEVICE_H
  //#include <stdio.h>
  #include <string.h>
  #include "boolean.h"
  #include "eeprom.h"
  
  BOOL read_device(char *buffer, unsigned int bytes, unsigned long int address);
  BOOL write_device(char *buffer, unsigned int bytes, unsigned int address);
  char read_byte(unsigned int address);
  BOOL write_byte(unsigned int address, char byte);
  unsigned long int get_device_size(void);

  BOOL device_create(char *imgpath, unsigned long int size);
  BOOL device_open(char *imgpath);
  void device_close(void);

#endif
