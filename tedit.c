#include "tedit.h"

void td_main(void){
 char ccharset[]= {0x00, 0x00, 0x04, 0x06, 0x1f, 0x06, 0x04, 0x00,
				   0x00, 0x00, 0x08, 0x0c, 0x0e, 0x0c, 0x08, 0x00,
                   0x00, 0x03, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
                   0x00, 0x1f, 0x00, 0x1f, 0x00, 0x1f, 0x00, 0x1f,
				   0x00, 0x10, 0x08, 0x04, 0x04, 0x14, 0x04, 0x14,
				   0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x03, 0x00,
				   0x00, 0x1f, 0x00, 0x1f, 0x00, 0x00, 0x1f, 0x00,
				   0x04, 0x14, 0x04, 0x14, 0x04, 0x04, 0x1f, 0x00};
							 
 char new [][17]={"Q: Create new?",
                   "  No",
				   "  Yes"};
				   
 char save[][17]={"Q: Save?",
                   "  Yes",
				   "  No"};
				   
// char ueeprom;
 char buffer [33];
 int n=0;
 char *fname;
 SFSFILE file;				
				
 CLI();   
 lcd_reset();
 load_ccharset(ccharset);
 lcd_init (2,1);
 lcd_home();
 
 putchar(2);
 putchar(3);
 putchar(4);
 lcd_move(16);
 putchar(5);
 putchar(6);
 putchar(7);
 
 lcd_move (4);
 cprints("Text Editor");
 //lcd_move (19);
 //cprints("2009,AFaustas");
 
 timer (65535);
 timer (65535);
 timer (65535);
 
 if (!sfs_check()){
 	  lcd_cls();
	  cprints (" W: EEPROM chk  failed! SFS=?");
	  alarm_snd();
	  return;
 }
 
 lcd_cls();
 cprints ("File name: ");
 lcd_move (16);
 cprints ("/");
 fname = input_dlg();
 /*lcd_move (16);
 cprints ("/");
 //CLI();
 SEI();
 kb_buffer=0;
 memset(buffer, 0, sizeof (buffer));
 while (n < 9){
  if (kb_buffer!=0){
   if (rq_code==0xB7) break;
   CLI();
   buffer[n++]=kb_buffer; //Trynimo galimybes reikia
   putchar (buffer[n-1]);
   kb_buffer=0;
   rq_code=0;
   SEI();
  }
 } */
 
 /* Atidaromas failas */
 file=sfs_fopen(fname, NOCREATE);
 if (!file){
   /* Jei nepavyko atidaryti (nerastas), tai klausiu, ar sukurti */
   if (menu (3, new)==2){ 
     if ((file = sfs_fopen(fname, SFS_ATTR_NOCD)) == 0){
       //E_FCF:
	   lcd_cls();
	   cprints ("File creation failed!!!");
	   kb_buffer=0;
	   while (!kb_buffer);
	   return;
	 }
   }
   else return;
 }
 
 //simplest TXT processor (MAX 32 bytes per file supported)
 //buffer[0]=0;
 lcd_init(2,1); 
 lcd_cls();
 memset(buffer, 0, sizeof (buffer)); //110112 bugfix
 sfs_fread (buffer, sizeof(buffer), &file);
 prints (buffer);
 
 kb_buffer=0;
 rq_code=0; //scancode
 n=0;

 while ((kb_buffer!='=') && (n < 32)){
  if ((kb_buffer!=0) && (kb_buffer != '=')){ 
   CLI();
   
   /* Jei buvo nuspausta DEL */
   if((kb_buffer == 0xFF) && (n > 0)){ 
     buffer[n--] = 0;
     lcd_putcmd(0x10); //Komanda, istrinanti simboli.
	 putchar (' ');  
	 lcd_putcmd(0x10);
	 kb_buffer = 0;
   }
   
   else{
     putchar(kb_buffer);
	 buffer[n++]=kb_buffer;
     kb_buffer=0;
     //putchar (buffer[n-1]);
   }
   SEI();
  }
 }
 
 buffer[n] = 0;
 if (menu (3, save)==1){
  int len = strlen (buffer);
  sfs_fwrite (buffer, strlen(buffer), &file);
 }
 return;
}
 
void timer (unsigned int c) {
  int x;
  for (x=0; x<c; x++)
    asm ("NOP");
}