CC = iccavr
LIB = ilibw
CFLAGS =  -IC:\iccv7avr\include\ -e -D__ICC_VERSION=722 -DATMega32  -l -g -MLongJump -MHasMul -MEnhanced -Wf-str_in_flash -Wf-const_is_flash -DCONST="" 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -O -LC:\iccv7avr\lib\ -g -e:0x8000 -ucrtatmega.o -bfunc_lit:0x54.0x8000 -dram_end:0x85f -bdata:0x60.0x85f -dhwstk_size:16 -beeprom:0.1024 -fihx_coff -S2
FILES = lcdlib.o mdump.o menu.o tedit.o calc.o calc_app.o sfs.o device.o 

CALC2:	$(FILES)
	$(CC) -o CALC2 $(LFLAGS) @CALC2.lk   -lcatmega
lcdlib.o: .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h .\boolean.h
lcdlib.o:	lcdlib.c
	$(CC) -c $(CFLAGS) lcdlib.c
mdump.o: .\mdump.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\eeprom.h .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h .\boolean.h .\menu.h
mdump.o:	mdump.c
	$(CC) -c $(CFLAGS) mdump.c
menu.o: .\menu.h C:\PROGRA~2\iccavr\include\iom32v.h .\lcdlib.h .\boolean.h
menu.o:	menu.c
	$(CC) -c $(CFLAGS) menu.c
tedit.o: .\tedit.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\_const.h .\menu.h C:\PROGRA~2\iccavr\include\iom32v.h .\lcdlib.h .\boolean.h .\sfs.h .\device.h .\..\..\..\..\..\..\PROGRA~2\iccavr\include\eeprom.h
tedit.o:	tedit.c
	$(CC) -c $(CFLAGS) tedit.c
calc.o: .\calc.h C:\PROGRA~2\iccavr\include\iom32v.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h .\boolean.h .\lcdlib.h .\calc_app.h .\menu.h .\tedit.h .\sfs.h .\device.h .\..\..\..\..\..\..\PROGRA~2\iccavr\include\eeprom.h .\affs.h
calc.o:	calc.c
	$(CC) -c $(CFLAGS) calc.c
calc_app.o: .\calc_app.h C:\PROGRA~2\iccavr\include\math.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h .\boolean.h .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h
calc_app.o:	calc_app.c
	$(CC) -c $(CFLAGS) calc_app.c
sfs.o: .\sfs.h .\boolean.h .\device.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\_const.h .\..\..\..\..\..\..\PROGRA~2\iccavr\include\eeprom.h
sfs.o:	sfs.c
	$(CC) -c $(CFLAGS) sfs.c
device.o: .\device.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\_const.h .\boolean.h .\..\..\..\..\..\..\PROGRA~2\iccavr\include\eeprom.h
device.o:	device.c
	$(CC) -c $(CFLAGS) device.c
