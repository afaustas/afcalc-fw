/* Naudoja calc_app.c */
#ifdef CALCAPP
#ifndef __CALC_APP_H
 #define __CALC_APP_H 
 
 #include <math.h>
 #include <macros.h>
 #include <stdlib.h>
 #include <string.h>
 
 #include "boolean.h"
 #include "lcdlib.h"
 
 #define CB_SIZE   	  			    64
 #define DEL 	   	  			    0xFF
 
 #define DEG						0
 #define RAD						1
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 extern BOOL calc_app; //For KB int. ISR. Kad suprastu, kad veikia calc_app
 char calc_buffer[CB_SIZE];
 char cb_pos = 0;
 volatile unsigned char mode = DEG;
 static float memory = 0;
 
 extern void beep (int kiek, int signalas);
 extern void timer (unsigned int c);
 extern char get_code (void); 
 void calc_main (void);
 void print_ats(float *ats);
 void InitCalc(void);
 void CalcAppLoop (void);
 void input_math_function(void);
 
 /* Naudoja naujas skaiciavimo algoritmas */
 
  enum veiksmai{LYGU,
			   SKLIAUSTAI_ATSIDARO,
			   SKLIAUSTAI_UZSIDARO,
			   SUDETIS,
			   ATIMTIS,
			   DAUGYBA,
			   DALYBA,
			   NEIGIMAS,
			   SIN,
				COS,
				TAN,
				ASIN,
				ACOS,
				ATAN,
				LOG,
				LAIPSNIU,
				SAKNIS
				};

 enum AtliktiSkaiciavimusRet{
								SUCCESSFUL = 0,
								BAD_EXPONENT,
								INVALID_EXPRESSION,
								INVALID_MATH_FUNCTION_INPUT
							};

							 	

 struct stack {float data;
			 struct stack *prev;};
 typedef struct stack STACK;

 void push (STACK **s, float data);
 float pop(STACK **s);
 void freestack(STACK **s);

 BOOL AddSk(char *data, unsigned int *x);
 BOOL Skaiciuoti(unsigned char mode);
 unsigned char AtliktiSkaiciavimus(char *data, float *result, char mode);
 
 STACK *snum;
 STACK *sop;
 /* ------------------------------------- */ 
#endif
#endif