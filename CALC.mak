CC = iccavr
LIB = ilibw
CFLAGS =  -IC:\PROGRA~1\icc\include -e -D__ICC_VERSION=722 -DATMega32  -l -A -g -MLongJump -MHasMul -MEnhanced -Wf-str_in_flash -Wf-const_is_flash -DCONST="" 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -LC:\PROGRA~1\icc\lib\ -g -e:0x8000 -ucrtatmega.o -bfunc_lit:0x54.0x8000 -dram_end:0x85f -bdata:0x60.0x85f -dhwstk_size:30 -beeprom:0.1024 -fcoff -S2
FILES = calc.o menu.o tedit.o lcdlib.o calc_app.o mdump.o affs.o 

CALC:	$(FILES)
	$(CC) -o CALC $(LFLAGS) @CALC.lk   -lstudio -lcatmega
calc.o: .\calc.h C:\PROGRA~2\iccavr\include\iom32v.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h .\boolean.h .\lcdlib.h .\calc_app.h .\menu.h .\tedit.h .\affs.h C:\PROGRA~2\iccavr\include\eeprom.h
calc.o:	calc.c
	$(CC) -c $(CFLAGS) calc.c
menu.o: .\menu.h C:\PROGRA~2\iccavr\include\iom32v.h .\lcdlib.h .\boolean.h
menu.o:	menu.c
	$(CC) -c $(CFLAGS) menu.c
tedit.o: .\tedit.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\_const.h .\menu.h C:\PROGRA~2\iccavr\include\iom32v.h .\lcdlib.h .\boolean.h .\affs.h C:\PROGRA~2\iccavr\include\eeprom.h
tedit.o:	tedit.c
	$(CC) -c $(CFLAGS) tedit.c
lcdlib.o: .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h .\boolean.h
lcdlib.o:	lcdlib.c
	$(CC) -c $(CFLAGS) lcdlib.c
calc_app.o: .\calc_app.h C:\PROGRA~2\iccavr\include\math.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h .\boolean.h .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h
calc_app.o:	calc_app.c
	$(CC) -c $(CFLAGS) calc_app.c
mdump.o: .\mdump.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h C:\PROGRA~2\iccavr\include\string.h .\lcdlib.h C:\PROGRA~2\iccavr\include\iom32v.h .\boolean.h .\menu.h
mdump.o:	mdump.c
	$(CC) -c $(CFLAGS) mdump.c
affs.o: .\affs.h C:\PROGRA~2\iccavr\include\eeprom.h C:\PROGRA~2\iccavr\include\string.h C:\PROGRA~2\iccavr\include\_const.h
affs.o:	affs.c
	$(CC) -c $(CFLAGS) affs.c
