#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define EOD 0
#define SKAICIUS 0x20
#define SUDETIS 1
#define ATIMTIS 2
#define DAUGYBA 3
#define DALYBA  4
#define SAKNIS  5

#define SIN 6

#define SKLIAUSTAI_ATSIDARO 126
#define SKLIAUSTAI_UZSIDARO 127
#define COMPLETED           125
#define IGNORE              124

char calc_buffer [50];
char i_cba[100];
float ii_cba[50];
char veiksmai [25];
char pvs; //Veiksmas ar skaicius pirma?
char n;
char lens=0;
char lent=0;
char ld=0;
char ts=0;
char eon=0; //End Of Number
float getss;
float tmp, ats;
char doc=0, nx=0;

float susk (float *ptr);
void skaiciuoti(void);
float kvadratu (int kiek, int kelintuoju);
char rastic (void);
void print_ats(void);

void main (){
START:
 ld=0;
 scanf ("%s", calc_buffer);
 skaiciuoti();
 //printf ("\n%Lf ", tmp);
 goto START;
 system ("pause");
 return;                        
}

void skaiciuoti(){
 for (n=0; n<50; n++){
   i_cba[2*n+1]=calc_buffer[n];
 }

 n=1;

 while (i_cba[n]!=EOD){ 
   if ((((i_cba[n]>=0x30) && (i_cba[n]<=0x39))||(i_cba[n]==0x2C))||(i_cba[n]==0x2E))//!
     i_cba[n-1]=SKAICIUS;
   else if (i_cba[n]=='+')
     i_cba[n-1]=SUDETIS;
   else if (i_cba[n]=='-')
     i_cba[n-1]=ATIMTIS;
   else if (i_cba[n]=='*')
     i_cba[n-1]=DAUGYBA;
   else if (i_cba[n]=='/') //Dalyba ant 2x16 LCD TURI KITOKI SIMBOLI!!!
     i_cba [n-1]=DALYBA;
   else if (i_cba[n]=='V') //Saknis!!!
     i_cba [n-1]=SAKNIS;
   else if ((i_cba[n]>=0x41)&&(i_cba[n]<=0x5A)){
     if  (i_cba[n] == 'S'){
       if (i_cba[n+2]=='I'){
         if (i_cba[n+4]=='N'){
           i_cba[n-1]=SIN;
           i_cba[n+1]=0xAA;
           i_cba[n+3]=0xAA;
         }
       }
     }
     //Kitos mat. f-jos.
   }
   else if (i_cba[n]=='(')
     i_cba[n-1]=SKLIAUSTAI_ATSIDARO;
   else if (i_cba[n]==')')
     i_cba[n-1]=SKLIAUSTAI_UZSIDARO;
   else{
     printf ("\nERROR!!!\n"); //Pakeisti. 2x16
     return;
   }
   n+=2;
 }

 //CON. Kitas puslapis (UML)

 //No ASCII.
 n=1;

 while (n){
   if (i_cba[n-1]==SKAICIUS)
     i_cba[n]-=0x30;    //Atimti ASCII offset.

   if (i_cba[n-1]==EOD)
     n=0;

   else
     n+=2;
 }

 //Convert to BIN.
 n=0;
 while(i_cba[n]!=0){
 while (1){
   if (i_cba[n]==SKAICIUS){
     if ((i_cba[n+1]<10)&&(i_cba[n+1]>=0)){
       ts=0;
       lens++;
       n+=2;
     }
     else {
       n+=2;
       while (i_cba[n]==SKAICIUS){
         lent++;
         n+=2;
         doc=1;
       }
     }
   }
   else
     break;
 }

//Sekantis puslapis: sveiku rasymas i long double
 ii_cba[ld]=0;

 while (lens>0){

  if (lens>1)
    getss=kvadratu(10, lens-1);
  else
    getss=1;

  ii_cba[ld]+=i_cba[(n-(lent+lens+doc)*2)+1]*getss;

  lens--;
  nx=1;
 }

 while (lent>0){
  getss=kvadratu(10, lent);
  ii_cba[ld]+=i_cba[(n-ts*2)-1]/getss;
  lent--;
  ts++;
 }
 if (nx==1){
  doc=0;
 }
 ld++;
 switch (i_cba[n]){
   case SKAICIUS:break;
   case SUDETIS: ii_cba[ld]=SUDETIS; n+=2; ld++; break;
   case ATIMTIS: ii_cba[ld]=ATIMTIS; n+=2; ld++; break;
   case DAUGYBA: ii_cba[ld]=DAUGYBA; n+=2; ld++; break;
   case DALYBA : ii_cba[ld]=DALYBA ; n+=2; ld++; break;
   case SIN    : ii_cba[ld]=SIN    ; n+=6; ld++; ii_cba[ld]=0; break;
   case SAKNIS : ii_cba[ld]=SAKNIS ; n+=2; ld++; break;

   case SKLIAUSTAI_ATSIDARO: ii_cba[ld]=SKLIAUSTAI_ATSIDARO; n+=2; ld++; break;
   case SKLIAUSTAI_UZSIDARO: ii_cba[ld]=SKLIAUSTAI_UZSIDARO; n+=2; ld++; break;

   default: n+=2; //Gali sukelti klaidas!!!
 }
 nx=0;
 }  //Perkoduoja ASCII i BIN (long double ii_cba[50]), suraso veiksmus i ten pat. [PABAIGA]

    //ii_cba[]==SVSVSVSVSVSVS...
    //Toliau reikia, kad butu atliekami skaiciavimai. (Pirma skliaustai), math.h!
    //Man reikalingi kintamieji: ii_cba[], ld, tmp
    tmp=susk(ii_cba);
    ats=tmp;
   print_ats();
/* for (ld=1; ii_cba[ld]; ld+=2){    //Kas antra...
    if (ii_cba[ld]==SKLIAUSTAI_ATSIDARO){
      while (ii_cba[ld]!=SKLIAUSTAI_UZSIDARO){
        if (ii_cba[ld]==SUDETIS){
          ...
*/
}



float kvadratu (int kiek, int kelintuoju){
  float ats;
  ats=1; //Dauginant is 0 gaunamas 0.
  for (; kelintuoju; kelintuoju--){
    ats*=kiek;
  }
  return ats;
}

float susk (float *ptr){        //Support expressions like: a+b/c=?
   char n=1, x;
   float tmp=0;
//   while (((ptr[n]<=16)||((ptr[n]==COMPLETED||ptr[n]==IGNORE)))||ptr[n]!=0){   //Kol suprantama.
     for (n=1; ptr[n]; n+=2){ //iesko * /
       if (ptr[n]==DAUGYBA){
         x=rastic();
         if (x==0)
           ptr[n-1]=ptr[n-1]*ptr[n+1];
         else {
           ptr[n-1]=ptr[x-1]*ptr[n+1];
           ptr[x]=IGNORE;
         }
         ptr[n]=COMPLETED;
       }
       else if (ptr[n]==DALYBA){
         x=rastic();
         if (x==0)
           ptr[n-1]=ptr[n-1]/ptr[n+1];
         else if (x<n) {
           ptr[n-1]=ptr[x-1]/ptr[n+1];
           ptr[x]=IGNORE;
         }
         else {
           ptr[n-1]=ptr[n-1]/ptr[x-1];
           ptr[x]=IGNORE;
         }
         ptr[n]=COMPLETED;
       }
     }                        //Paieska baigta.
     for (n=1; ptr[n]; n+=2){
       if (ptr[n]==SUDETIS){
         x=rastic();
         if (x==0)
           ptr[n-1]=ptr[n-1]+ptr[n+1];
         else {
           ptr[n-1]=ptr[x-1]+ptr[n-1];
           ptr[x]=IGNORE;
         }
         ptr[n]=COMPLETED;
       }

       else if (ptr[n]==ATIMTIS){
         x=rastic();
         if (x==0)
           ptr[n-1]=ptr[n-1]-ptr[n+1];
         else if (x<n) {
           ptr[n-1]=ptr[x-1]-ptr[n+1];
           ptr[x]=IGNORE;
         }
         else {
           ptr[n-1]=ptr[n-1]-ptr[x-1];
           ptr[x]=IGNORE;
         }
         ptr[n]=COMPLETED;
         ptr[n+1]=ptr[n-1];
       }
     }
//   }
  for (n=1; ptr[n]!=COMPLETED; n+=2);
  return ptr[n-1];
  /* Negali atlikti veiksmu panasiu i si: 2*3/2+15=5. Pakimba jei irasytos bent
     kokios raides. */
}

char rastic (void){
  char x;
  for (x=1; ii_cba[x]; x+=2){
    if (ii_cba[x]==COMPLETED)
      return x;
  }
  return 0;
}

//void print_ats(){

void print_ats(void){
/*        char *p, pr ;
        int a, z, t;
	int i;
        n=sizeof(ats);
        p=&ats;   //gaunu ld adresa.
	for (i = 0; i < n; i++) {
          pr=0xff & p[i];
          a=pr&0xf0;
          a/=16;
          z=pr&0x0f;
          t=0;
          while (1){
           if (t==0){
             pr=a;
           }
           else if (t==1){
             pr=z;
           }
           switch (pr){
             case 0x00: putchar ('0'); break;
             case 0x01: putchar ('1'); break;
             case 0x02: putchar ('2'); break;
             case 0x03: putchar ('3'); break;
             case 0x04: putchar ('4'); break;
             case 0x05: putchar ('5'); break;
             case 0x06: putchar ('6'); break;
             case 0x07: putchar ('7'); break;
             case 0x08: putchar ('8'); break;
             case 0x09: putchar ('9'); break;
             case 0x0A: putchar ('A'); break;
             case 0x0B: putchar ('B'); break;
             case 0x0C: putchar ('C'); break;
             case 0x0D: putchar ('D'); break;
             case 0x0E: putchar ('E'); break;
             case 0x0F: putchar ('F'); break;
           }
           if (t==1)
             break;
           t++;
          }

        }     */
  char isvesti[14];
  //ftoa(ats, isvesti);
  //lcd_move (20);
  printf ("%f", ats);
}

