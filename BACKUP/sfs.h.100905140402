/* 

	Original file name: sfs.h
	Name			  : SimpleFS filesystem driver functions
	Version			  : v1.0
	Date			  : 2010-09-05
	Compiler		  : ImageCraft ICCAVR

			  (c)Copyleft 2010, Azuolas - Faustas BAGDONAS

*/

#pragma once
#ifndef __SFS_H
  #define __SFS_H

  #include <time.h>
  #include "boolean.h"
  #include "device.h"

  #define SFS_ATTR_INVALID	0
  #define SFS_ATTR_PUSHDIR	1
  #define SFS_ATTR_POPDIR	2
  #define SFS_ATTR_NOCD		3
  #define NOCREATE          5
  
  #define VOLUMESPEC        "SFS V00.01\0\0"
  #define HEAD              "HEAD"
  #define TAIL              "TAIL"
  #define FILE              "FILE"
  #define BYTE_ORDER        0x04030201
 
  typedef unsigned long int SFSFILE; 

  /* MACROS */
  /* ---------------------------------------------
   * Macro    : sfs_file_exist(), PUBLIC
   * Argument.: Failo vardas.
   * Paskirtis: Patikrinti ar failas egzistuoja.
   * Return   : TRUE, jei egzistuoja. 
   * --------------------------------------------- */
  #define sfs_file_exist(fname)   (sfs_fopen(fname, NOCREATE) ? TRUE : FALSE)
  
  /* FUNCTION PROTOTYPES */

  /* PUBLIC FUNCTIONS */
  BOOL sfs_format(void);
  unsigned long int sfs_listdir(unsigned long int fspos, 
                                char *ptrfnamebuf, 
			                    unsigned char fnamebufsz);
  SFSFILE sfs_fopen(char *fname, char creation_attrib);
  unsigned long int sfs_fread(char *buf, unsigned long int size, SFSFILE *file);
  BOOL sfs_fwrite(char *buf, unsigned long int size, SFSFILE *file);
  //unsigned int sfs_usedspace(void);
  BOOL sfs_check(void);
  BOOL sfs_remove (char *fname);
  BOOL sfs_format(void);
  unsigned long int sfs_freespa(void);
  unsigned long int sfs_get_filesz(SFSFILE file);
  char sfs_get_file_headersz(SFSFILE file);
  SFSFILE sfs_seek(SFSFILE file, BOOL to_begin);

  /* PRIVATE FUNCTIONS */
  BOOL sfs_alloc(unsigned long int pos, unsigned long int size);
  void sfs_free(unsigned long int pos, unsigned long int size);
  unsigned long int sfs_start_freespa(void);
  BOOL sfs_chk_fname(char *fname);

#endif
