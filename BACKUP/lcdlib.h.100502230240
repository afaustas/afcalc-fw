/* 
        Original file name:    lcdlib.h
        Name:                  Alphanumberic LCD driver (2x16)
        Version:               v1.1
        Date:                  2008-08-14
        Compiler:              ImageCraft IDE
               
        Description: Tekstinio dvieju eiluciu (2x16) LCD displejaus draiveris.
Draiveris turi sias funkcijas:

        1. printf ("String")        - atspausdina stringa ekrane.
        2. lcd_init (lines, cursor) - LCD initializacija, lines - eiluciu skaicius
(1 arba 2), cursor, jei = 0 - be kursoriaus, o jei = 1, tai su kursorium.
        3. lcd_reset()              - nuresetina LCD
        4. lcd_turn_off()           - isjungia lcd
        5. lcd_disp_cursor(value)   - ekrane rodomas kursorius jei reiksme = 1
        6. lcd_font(value)          - Jei reiksme 1, tai fontas didesnis.
        7. lcd_cls()                - Isvalo LCD (CLS)
        8. lcd_send_cmd(command)    - Siusti komanda
        9. lcd_send_data(data)      - Siusti duomenis
       10. putchar(char)            - Isvesti simboli.      
       11. lcd_move(x, y)           - Perkelti kursoriu i koordinates x ir y.
       12. lcd_turn_on()            - Ijungia LCD.
       13. lcd_home()               - Grazina LCD kursoriu i pradzia
       

           Written by Azuolas BAGDONAS, (c) 2008 - 2010
*/

#ifndef __lcdlib_h
  #define __lcdlib_h 

#include <iom32v.h>
#include "boolean.h"

#pragma device_specific_function putchar  

#define LCD   		  		   24
#define WAIT  				   300   

#define EIGHT_BIT_TWO_LN       0x38 
#define EIGHT_BIT_ONE_LN       0x30 
#define ON_NOCURSOR_NO_UNDERLN 0x0C 
#define ON_NOCURSOR_UNDERLN    0x0E 
#define ON_CURSOR_NO_UNDERLN   0x0D 
#define ON_CURSOR_UNDERLN      0x0F
#define OFF                    0x08 
#define CURSOR_HOME            0x02 
#define CLS                    0x01 
#define RESET                  0x30 
#define CBACK                  0x10
#define CFWD                   0x14
#define CGRAM_LOAD             0x40
#define SLINE                  0x07
#define NORMAL_PRINTING        0x06
#define RESET_DDRAM_ADDR       0x80

#define RS       0x80
#define E        0x40 
#define RS_E     0xC0
#define RW       0x20  
#define AND_E    0xBF  
#define AND_RS_E 0x3F  
#define AND_RS   0x7F

void cprints (const char *string);
void prints (char *string);
void lcd_move (char x); 
void putchar (char c);
void lcd_init (char ln, char cursor); 
void lcd_reset (void);
void lcd_off (void); 
void lcd_turn_on (void);
void lcd_cursor (char type); //012
void lcd_font (char size); 
void lcd_cls (void); 
void delst(void); 
void lcd_home(void); 
void lcd_putcmd(char cmd);
void lcd_send_data (char data); 
void load_ccharset (char *simb);
void load_cchar (char *csimb, char adr);
void lcd_cursor_fwd (char fwd);
void lcd_cursor_back (char back);
void clcd_hsl (const char *str, unsigned int timeout);
void lcd_hsl (char *str, unsigned int timeout);
void clcd_ssl (const char *str, unsigned int timeout, char from, char to, BOOL interruptable);
void lcd_ssl (char *str, unsigned int timeout, char from, char to, BOOL interruptable);

/* Update 1.1 */
void lcd_backup_ccharset(unsigned char *where);
void lcd_backup_data(unsigned char *where, unsigned char num_of_chr);
void lcd_restore_data(unsigned char *data, unsigned char num_of_chr);
unsigned char lcd_get_cursor_pos(void);
void lcd_set_cursor_pos(unsigned char pos);

#endif