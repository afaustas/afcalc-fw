/* 

	Original file name: sfs.c
	Name			  : SimpleFS filesystem driver functions
	Version			  : v1.0
	Date			  : 2010-09-05
	Compiler		  : ImageCraft ICCAVR or MS Visual Studio 2008

	Description: SimpleFS failu sistemos draiverio funkcijos. Parengta
	remiantis http://www.star.bnl.gov/public/daq/DAQ1000/sfs.pdf  (SFS
	dokumentacija). Stengiausi nenukrypti nuo  dokumentacijos,  taciau
	gali buti neatitikimu. Dali headeruose esanciu duomenu, pavyzdziui
	Byte Order sios funkcijos ignoriuoja.Galbut veliau tai istaisysiu,
	o dabar tai bent leidzia sumazinti programines irangos apimti. Sis
	draiveris  [ dar ] neturi direktoriju palaikymo, failu pervadinimo
	funkciju. Galbut veliau tai isspresiu,jei liks vienos mikrokontro-
    leryje.

			  (c)Copyleft 2010, Azuolas - Faustas BAGDONAS

*/

#include "sfs.h"

/* GLOBAL VARRIABLES */
char VolumeSpec[] = VOLUMESPEC;
char Head[] = HEAD;
char Tail[] = TAIL;
char File[] = FILE;
unsigned long int byte_order = BYTE_ORDER;

/*        P U B L I C   F U N C T I O N S        */

/* ---------------------------------------------
 * Funkcija : sfs_format()
 * Paskirtis: SFS formatavimas
 * Return   : TRUE, jei buvo sekmingas. 
 * --------------------------------------------- */
BOOL sfs_format(void){
  /* tail_start nurodo, kur prasideda tail irasas */
  unsigned int x;
  unsigned int tail_start = get_device_size() - 12;
  unsigned long int timer = 0x55AA55AA; //Kolkas taip. Veliau gal random()

  /* Rasomas VolumeSpec ir Header'is */
  write_device(VolumeSpec, 12, 0);
  write_device(Head, 4, 12);
  write_device((char*)&byte_order, 4, 16);
  write_device((char*)&timer, 4, 20); //Ateityje pagalvoti apie time.
  
  /* Kitkas uzpildoma vienetais */
  for (x = 25; x < tail_start; x++)
    write_byte(x, 0xFF);

  /* Rasomas TAIL blokas */
  write_device(Tail, 4, tail_start);
  write_device((char*)&byte_order, 4, tail_start + 4);
  write_device((char*)&timer, 4, tail_start + 8); //!!!
  
  return sfs_check();
}

/* ---------------------------------------------
 * Funkcija : sfs_check()
 * Paskirtis: SFS patikrinimas nuo klaidu
 * Return   : TRUE, jei buvo sekmingas. 
 * --------------------------------------------- */
BOOL sfs_check(void){
  char buf[12];
  unsigned long timer;
  unsigned long *sz;
  unsigned char head_sz;
  unsigned char attrib;
  BOOL ach_tail=FALSE;
  BOOL ach_free_spa = FALSE;
  unsigned long int pos;
  unsigned long int devsz = get_device_size();
  char fname[240];

  /* Tikrinu VolumeSpec ir Head */
  read_device(buf, 12, 0);
  if (memcmp(VolumeSpec, buf, 5))
    return FALSE;
  read_device(buf, 12, 12);
  if (memcmp(Head, buf, 4))
    return FALSE;

  /* Cia galima dirbti su byte order, bet kolkas tuscia,  *
   * nes mano atveju jis nereikalingas GALBUT...          */

  timer = *((unsigned long int*)&buf[8]); 
  
  pos = 24;
  /* Tikrinu kas yra po head. Cia turetu buti failai.     */
  while ((!ach_tail) && (pos < devsz)){
    read_device(buf, 12, pos);
    /* Ar radau faila? */
    if (!memcmp(File, buf, 4)){
      /* Ignoruoju byte_order, skaitau failo dydi                    */
      sz = (unsigned long int *)&buf[8];
      pos += 12;
      head_sz = read_byte(pos++);
      /* Jei netinkamas headerio dydis, tai FAILED                   */
      if ((head_sz % 4)||(head_sz < 20)) return FALSE;
      attrib = read_byte(pos++);            //Skaitau atributus
      /* Jei failas istrintas, tai reiskia jau toliau ju nebebus     */
      if (attrib == SFS_ATTR_INVALID){ 
        ach_free_spa = TRUE;
	    continue;
      }
      if (ach_free_spa) return FALSE;
      pos+=2;                               //Ignoruoju RESERVED
      /* Tikrinu ar failo vardas yra tinkamas ir jis nesidubliuoja   */
      read_device(fname, head_sz - 16, pos);
      if (!sfs_chk_fname(fname)) return FALSE;
      pos += ((unsigned long int)head_sz - 16 + *sz);
    }  
    /* Ar radau TAIL'a */
    else if (!memcmp(Tail, buf, 4)){
      pos += 12;
      /* Ignoruoju byte_order ir palyginu timer */
      if (memcmp((unsigned long int*)&buf[8], &timer, 4)) return FALSE;
      //if (pos + 1 != get_device_size()) return FALSE; - tikrinu pab.
      ach_tail = TRUE;
      continue;
    }
    /* Kitas atvejis: rasta tuscia vieta */
    else {
      ach_free_spa = TRUE;
      pos ++;
    }
  }
  
  /* Jei pavyko pasiekti ir patikrinti TAIL ir jis yra pabaigoje,
     tai viskas gerai                                             */
  if (ach_tail && (pos == devsz)) return TRUE;

  return FALSE;
}

/* ---------------------------------------------
 * Macro  !!!  : sfs_file_exist(), PUBLIC
 * Argument.: Failo vardas.
 * Paskirtis: Patikrinti ar failas egzistuoja.
 * Return   : TRUE, jei egzistuoja. 
 * --------------------------------------------- *
BOOL sfs_file_exist(char *fname){
  return sfs_fopen(fname, NOCREATE) ? TRUE : FALSE;      -dabar kaip macro
}*/

/* ---------------------------------------------
 * Funkcija : sfs_fopen()
 * Argument.: 1. Failo vardas
 *            2. Atributas, jei reikia  sukurti
                 nauja faila. Jei nereikia kur-
				 ti, tai NOCREATE.
 * Paskirtis: Failo atidarymas ar/ir sukurimas.
 * Return   : Faile esanciu duomenu pradzios ad-
 *            resas.
 * --------------------------------------------- */
SFSFILE sfs_fopen(char *fname, char creation_attrib){
  unsigned long int startpos = 0;
  unsigned long int end = get_device_size();
  BOOL create = (creation_attrib != NOCREATE);
  char mod;
  char headsz;
  unsigned long int size = 0;
  unsigned char c;

  /* Konvertuoju failu vardus i mazasias raides. Nebus case sensitive. */
  for (c = strlen(fname) - 1; c != 255; c--){
	  if ((fname[c] >= 'A') && (fname[c] <= 'Z'))
		  fname[c] += ('a' - 'A');
  }

  /* Jei reikia sukurti nauja faila */
  if (create){
	if (!(sfs_chk_fname(fname) && !sfs_file_exist(fname))) return 0;
    startpos = sfs_start_freespa();
    /* Jei dar yra laisvos vietos */
    if (startpos + 16 + strlen(fname)+4 < end){
      write_device(File, 4, startpos);
      write_device((char*)&byte_order, 4, startpos + 4);
      write_device((char*)&size, 4, startpos + 8); //Size = 0
      /* Skaiciuoju headsz. Jei jis nesidalija is 4, tai pridedu *
       * kelis baitus, kad dalintusi.                            */
      headsz = 16 + strlen(fname) + 1;
      if (mod = (headsz % 4))
        headsz += 4 - mod;
      startpos += 12;    
      write_byte(startpos++, headsz);
      write_byte(startpos++, creation_attrib);
      write_byte(startpos++, 0);
      write_byte(startpos++, 0);
      write_device(fname, headsz - 16, startpos);
      startpos += headsz - 16;
    }
  }
  /* Jei reikia atidaryti jau esanti faila */
  else{
    unsigned long int pos = 0;
    char buf[4];
    char headsz;
	char curfname[64]; ///!!!GALIMA DINAMISKAI, PAGAL strlen(fname).
      
    startpos = 0;

    for (pos = 0; pos < end; pos++){
      read_device(buf, 4, pos);
      /* Tikrina ar rastas buvo failas ir ar jis egzistuoja (neistrintas) */
      if ((!memcmp(File, buf, 4)) && (read_byte(pos+13) != SFS_ATTR_INVALID)){
		headsz = read_byte(pos + 12);
		read_device(curfname, strlen(fname) + 1, pos + 16);
		if (!memcmp(curfname, fname, strlen(fname))){
		  startpos = pos + headsz;
		  break;
		}
      }
    }
  }
  return startpos;
}

/* ---------------------------------------------
 * Funkcija : sfs_fwrite()
 * Argument.: 1. Buferis, is kurio skaityti
 *            2. Rasomu baitu kiekis
 *            3. Failo handler'is
 * Paskirtis: Duomenu rasymas i faila
 * Return   : Sekmingumas. TRUE - sekmingas.
 * --------------------------------------------- */
BOOL sfs_fwrite(char *buf, unsigned long int size, SFSFILE *file){
  unsigned long int x, endpos;
  unsigned char fbuf[4] = {0};
  BOOL success = FALSE;

  /* Siame cikle ieskau failo header'io FILE */
  for (x = ((unsigned long int)*file)-1; (x && (memcmp(File, fbuf, 4))); x--)
    read_device(fbuf, 4, x);    
  /* Failo header'io pradzia jau radau, tikrinu ar failas tinkmamas *
   * ir po rasymo nevirsis disko dydzio                             */
  if((read_byte(x + 14) == SFS_ATTR_NOCD) && 
     (*file + size < get_device_size() - 12)&& x){
    /* Rezervuoju vieta rasomiems duomenims tik tada, jei append'inama */
    endpos = sfs_get_filesz(*file) + x + 1;
	if (*file + size > endpos){ 
	  /* Jei truksta vietos, tai FAIL */
      if (!sfs_alloc(*file, *file + size - endpos))
		return FALSE;
	  else{
		/* Atnaujinu size header'yje, jei pavyko rezervuoti vietos */
		read_device(fbuf, 4, x + 9);
		*((unsigned long int*)&fbuf) += size;
		write_device(fbuf, 4, x + 9);
	  }
	}
    /* Rasau duomenis */
    write_device(buf, size, *file);

	/* Increment'inu pozicija */
	*file += size;

    success = TRUE;
  }

  return success;
}

/* ---------------------------------------------
 * Funkcija : sfs_fread()
 * Argument.: 1. Buferis, i kuri rasyti duomenis
 *            2. Skaitomu baitu kiekis
 *            3. Failo handler'is
 * Paskirtis: Duomenu skaitymas is failo
 * Return   : Bytes read.
 * --------------------------------------------- */
unsigned long int sfs_fread(char *buf, unsigned long int size, SFSFILE *file){
  unsigned long int eof_pos, x;
  char fbuf[4] = {0};

  /* Skaiciuoju failo pabaigos pozicija failu sistemoje */
  for (x = ((unsigned long int)*file) - 1; x && memcmp(File, fbuf, 4); x--)
    read_device(fbuf, 4, x);
  eof_pos = x + sfs_get_filesz(*file - 1);

  /* Randu duomenu pradzia faile - sita turi daryt fopen!!! -lyg ir daro*/
  //*file += sfs_get_file_headersz(*file);

  /* Skaitau faila */
  for (x = 0; (x < size) && (*file <= eof_pos); x++){
    buf[x] = read_byte((*file)++);
  }

  return x; 
}

/* ---------------------------------------------
 * Funkcija : sfs_get_filesz()
 * Argument.: 1. SFS File handler (pos. in FS)
 * Paskirtis: Gauti failo dydi.
 * Return   : Failo dydis.
 * --------------------------------------------- */
unsigned long int sfs_get_filesz(SFSFILE file){
  unsigned long int size, x;
  char buf[4];
  char headsz;

  read_device(buf, 4, file - 1); //file

  for (x = ((unsigned long int) file) - 1; x && memcmp(File, buf, 4); x--)
    read_device(buf, 4, x);

  read_device((char*)&size, 4, x + 9);
  headsz = read_byte(x + 13);

  size += (unsigned long int) headsz;

  return size;
}

/* ---------------------------------------------
 * Funkcija : sfs_get_file_headersz()
 * Argument.: 1. SFS File handler (pos. in FS)
 * Paskirtis: Gauti failo header'io dydi.
 * Return   : Failo header'io dydis.
 * --------------------------------------------- */
char sfs_get_file_headersz(SFSFILE file){
  unsigned long int x;
  char buf[4];

  read_device(buf, 4, file);

  for (x = ((unsigned long int) file - 1); x && memcmp(File, buf, 4); x--)
    read_device(buf, 4, x);

  if (!x) return 0;
  return read_byte(x + 13);
}

/* ---------------------------------------------
 * Funkcija : sfs_freespa()
 * Paskirtis: Suzinoti, kiek liko laisvos vietos
 * Return   : Laisva vieta (baitais).
 * --------------------------------------------- */
unsigned long int sfs_freespa(void){
  return get_device_size() - 12 - sfs_start_freespa();
}

/* ---------------------------------------------
 * Funkcija : sfs_seek()
 * Argument.: 1. File handler
 *            2. Seek'inti i pradzia. Jei TRUE,
 *               tai bus seek'inama i pradzia, o
 *               jei FALSE, tai i failo pabaiga.
 * Paskirtis: Keisti failo handler'io pozicija.
 * Return   : File handler'is su pakeista pozi-
 *            cija. Nesekmes atveju 0.
 * --------------------------------------------- */
SFSFILE sfs_seek(SFSFILE file, BOOL to_begin){
  unsigned long int x;
  char buf[4] = {0};

  /* Ieskau to failo FILE header'io */
  for (x = ((unsigned long int) file) - 1; x && memcmp(File, buf, 4); x--)
    read_device(buf, 4, x);

  /* Jei ji rasti pavyko, tai... */
  if (x){
    x++;
	if (to_begin)
		x += sfs_get_file_headersz(file);
	else
		x += sfs_get_filesz(file);
	//to_begin ? (x += sfs_get_file_headersz(file)) : (x += sfs_get_filesz(file));
  }
  
  return (SFSFILE)x;
}

/* ---------------------------------------------
 * Funkcija : sfs_listdir()
 * Argument.: 1. FS position. Jei 0,tai nuo pra-
 *               dziu.
 *            2. Pointeris i failo vardo buferi
 *            3. Failo vardo buferio dydis
 * Paskirtis: Gauti saknines direktorijos turini
 * Return   : Atnaujinta FS pozicija, kuria pa-
 *            sinaudojus galima gauti sekancio
 *            failo varda. Jei 0 - nera failu.
 * --------------------------------------------- */
unsigned long int sfs_listdir(unsigned long int fspos, 
                              char *ptrfnamebuf, 
			                  unsigned char fnamebufsz){
  unsigned long int x;
  char buf[4] = {0};
  unsigned char headsz;

  for (; fspos < get_device_size(); fspos++){
    read_device(buf, 4,fspos);
    /* Jei randu FILE irasa */
    if(!memcmp(File, buf, 4)){
      /* Skaitau failo vardo dydi */
      headsz = read_byte(fspos + 12);
      /* Skaitau failo varda... */
      for (x = 0; (x < ((unsigned long int)headsz - 16)) && (x < (unsigned long int)fnamebufsz-1); x++)
        ptrfnamebuf[x] = read_byte(fspos + 16 + x);
      /* Prie nuskaityto failo vardo pridedu '\0' */
      ptrfnamebuf[x] = 0;
      /* Padidinu fspos */ 
      fspos += headsz;
      return fspos;
    }
  }

  //if (fspos == get_device_size()){
  ptrfnamebuf[0] = 0;
  return 0;
  //}
}

/* ---------------------------------------------
 * Funkcija : sfs_remove()
 * Argument.: 1. Trinamo failo vardas
 * Paskirtis: Istrinti faila
 * Return   : TRUE, jei pasiseke.
 * --------------------------------------------- */
BOOL sfs_remove (char *fname){
	SFSFILE f = sfs_fopen(fname, NOCREATE);
	BOOL success = FALSE;
	unsigned long int x, filesz;
	char buf[4] = {0};
  
	/* Jei trinamas failas egzistuoja, tada ji trinu - keiciu atributa arba
	   perkeliu toliau esancius duomenis i jo vieta                          */
	if (f){
		/* Tikrinu ar sis failas ne paskutinis failu sistemoje */
		for (x = ((unsigned long int)f) - 1; (x < get_device_size()) && (memcmp(File, buf, 4)); x++)
			read_device(buf, 4, x);
		x++;

		/* Jei tai paskutinis failas */
		if (x == get_device_size() - 1 ){ //Bugfix 110113
			f -= sfs_get_file_headersz(f);
			write_byte(f + 13, SFS_ATTR_INVALID); 
		}

		/* Jei dar yra daugiau failu, tai pasalinu faila ir perkeliu uz jo
		   esancius duomenis                                                 */
		else{
			/* Ieskau trinamo failo pradzios */
			for (x = (unsigned int)f, buf[0] = 0; x && memcmp(File, buf, 4); x--)
				read_device(buf, 4, x);
			x++;
			filesz = sfs_get_filesz(f);
			sfs_free(x, filesz);
		}
		success = TRUE;
	}
	return success;
}

/*       P R I V A T E   F U N C T I O N S       */

/* ---------------------------------------------
 * Funkcija : sfs_chk_fname()
 * Argument.: Failo vardas.
 * Paskirtis: SFS failo vardo  teisingumo  patik-
 *            rinimas. Varda gali sudaryti simbo-
 *            liai 'A'-'Z', 'a'-'z', '1'-'9', '/'
 *            '.', '_', '-'.
 * Return   : TRUE, jei pavadinimas teisingas. 
 * --------------------------------------------- */
BOOL sfs_chk_fname(char *fname){
  BOOL correct = FALSE;
  unsigned char x;
  for (x = 0; fname[x]; x++){
    if ((fname[x] >= '0') && (fname[x] <= '9'));
    else if (fname[x]=='/'||fname[x]=='.'||fname[x]=='_'||fname[x]=='-');
    else if (((fname[x] & 0xDF) >= 'A') && ((fname[x] & 0xDF) <= 'Z'));
    else break;
  }
  
  if (x == strlen(fname)){
    correct = TRUE;
  }

  return correct;
}

/* ---------------------------------------------
 * Funkcija : sfs_alloc()
 * Argument.: 1. Pozicija failu sistemoje
 *            2. Rezervuojamas dydis
 * Paskirtis: Rezervuoti vieta failu sistemoje
 * Return   : Sekmingumas. TRUE - sekmingas.
 * --------------------------------------------- */
BOOL sfs_alloc(unsigned long int pos, unsigned long int size){
  unsigned long int data_end = sfs_start_freespa();
  unsigned long int new_data_end = data_end + size;

  /* Patikrinu ar yra vietos */
  if (sfs_freespa() < size) 
	return FALSE;

  /* Rezervuoju vieta failu sistemoje, jei vietos pakankamai */
  for (; data_end >= pos; data_end--){
    write_byte(new_data_end, read_byte(data_end));
    new_data_end--;
  }
    return TRUE;
}

/* ---------------------------------------------
 * Funkcija : sfs_free()
 * Argument.: 1. Pozicija failu sistemoje
 *            2. Atlaisvinamas dydis
 * Paskirtis: Atlaisvinti vieta failu sistemoje.
              Uz atlaisvinamu duomenu bloko esan-
			  tys duomenys perkeliami i atlaisvi-
			  namo bloko pradzia.
 * --------------------------------------------- */
void sfs_free(unsigned long int pos, unsigned long int size){
  unsigned long int x;

  /* Atlaisvinu vieta failu sistemoje */
  for (x = pos; x + size < (get_device_size() - 12); x++){ //Bugfix 110113
	  write_byte(x, read_byte(x + size));
	  /* Perkeles istrinu senus duomenis, kad nebutu problemu */
	  write_byte(x + size, 0xFF);
  }
  
}

/* ---------------------------------------------
 * Funkcija : sfs_start_freespa()
 * Paskirtis: Rasti laisvos vietos pradzios ad-
 *            resa.
 * Return   : Laisvos vietos pradzios adresas.
 * --------------------------------------------- */
unsigned long int sfs_start_freespa(void){
  unsigned long int start_free = get_device_size() - 12; //12 bytes TAIL rec.
  unsigned long int last_file_size;
  char buf[4] = {0};
  char headsz;
  
  /* Ieskau paskutinio failo pradzios */
  for (; start_free; start_free--){
    read_device(buf, 4, start_free);
    /* Jei rastas failas, tikrinti ar jis neistrintas */
    if(!memcmp(File, buf, 4)){
      /* Jei neistrintas, tai pridedu dar jo dydi ir gaunu ats. */
      if (read_byte (start_free + 13) != SFS_ATTR_INVALID){
        read_device((char*)&last_file_size, sizeof(unsigned long int), start_free + 8);
		headsz = read_byte(start_free + 12);
		start_free += (last_file_size + headsz);
		return start_free;
      }
    }
  }
  
  /* Jei failu nera, tai pradzia yra ten, kur galima rasyti pirmaji faila */
  return 24;
}

/* -------------------[ EOF ]---------------------- */