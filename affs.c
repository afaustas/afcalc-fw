#include "affs.h"
#include <string.h>

char check_affs (void){
  char chk[3];
  EEPROMReadBytes(0x0000, chk, 3); 
  if (cmemcmp(AEOF, chk, 3))
    return 1;
  return 0;
}

void affs_list_files(FILEA *list){   //FILEA list - masyvas is 10 elementu
  int address=0x0003, faddress;
  char buffer[9];
  // FILEA read;
  char fnr=0;
  buffer[8]=0; //str. terminator
  buffer[4]='a';buffer[5]='z';

  while ((address<=103)&&(fnr<10)){
    EEPROMReadBytes(address, buffer, 8); //Read to temporary.
    address+=8;
    EEPROMReadBytes(address, &faddress, 2);
    address+=2;
    strcpy (list[fnr].name, buffer);
    list[fnr].address=faddress; 
/*    while (x<=10){
      if (x==' '){
        x++;
	    continue;
	  }
      read.name[n][x]=buffer[x];
      x++;
    } */
  
//    read.address[n]=faddress;
  
//   n++;
    fnr++;
  }
//  return read;
}

void affs_format(void){
  int addr;
  char write[]="\xAF\xAB\xAF";
  
  //erasing...
  
  for (addr=0; addr<=512; addr++)
    EEPROMwrite( addr, 0x00);
  
  //Creating affs structure...
  
  EEPROMWriteBytes(0x0000, write, 3);
  
}

char affs_new_file (char *name){
  int haddr=3;
  int baddr=103;
  char stt, aeof[]=AEOF;
  do{
    stt=EEPROMread(haddr);
    haddr+=10;
  }
  while ((haddr<103)&&stt);  
  if (haddr > 10)
    haddr-=10;

  if ((haddr==93)&&(EEPROMread(haddr)>0)){        //
    if (EEPROMread(haddr+1)>0)
      return 0;
  }
  //--
  {
//  char scan=0;
//  char bukle=0;
    char buff[6];

    while ((baddr>0)&&(baddr<510)){
      EEPROMReadBytes(baddr, buff, 6);

      if ((baddr==103&&buff[0]==0)&&buff[1]==0){
     //   baddr=103;
        break;
      }

      if (!(memcmp(buff, (char *)"\xaf\xab\xaf\xab\x00\x00", 6))){
        baddr+=4;
        break;
      }

      baddr++;

      if (baddr >= 510)
        baddr=0;
    }
  }
  //---
  EEPROMWriteBytes (haddr, cfname(name), 8);
  EEPROMWriteBytes (haddr+8, (char *)&baddr, 2);
  EEPROMWriteBytes (baddr, aeof, 4);
  return 1;
}

char *cfname (char *name){        //flush buff?
  static char nname[9];
  char cnn=0, cn=0;
  for (; cnn<9; cnn++) nname[cnn]=0;
  cnn=0;
  memset(nname, 0, 9);
  while(1){
    if (name[cn]=='.'){
      if (cnn<5){
        nname[cnn]=' ';
        cnn++;
      }
      else
        cn++;
      continue;
    }
    else if ((name[cn]!=0)&&(cnn<=8)){
      nname[cnn]=name[cn];
      cnn++; cn++;
      if (cnn>8)
        return &nname[0]; //???!!!
    }
    else{
      cnn++;
      if (cnn<9){
        nname[cnn]=' ';
        cnn++;
      }
      else 
        return &nname[0];
    }
  }
  return 0;
}

int affs_openfile(char *aname){
  char *name;
  int addr;
  char buff[10];
  int stad=3;

  while (1){
    EEPROMReadBytes(stad, buff, 10);
    stad+=10;

    name=cfname(aname);

    if (!(memicmp(name, buff, 8))){  //Jei 0, tai sutampa. CHG 7
      addr=buff[8]+buff[9]*256;
      return addr;
    }

    else if (stad>103)
      return 0;
  }
  return 0;
}

int affs_usedspace(void){
  return 512-affs_freespace();
}

int affs_freespace(void){
  int addr=511, free=0;
  while (!(EEPROMread(addr))){
    addr--;
    free++;
  }
  return free;
}

int affs_findfree(void){
  int addr=511;
  if ( (!(EEPROMread(101)))&&(!(EEPROMread(102)))){
    while (!(EEPROMread(addr)))
      addr--;
    return addr;
  }
  else
    return 0;
}

void affs_read (int stream, char *data){    //data - 490 baitu masyvas.
  int n=0;
  unsigned char baitas;
  while (1){
    baitas=EEPROMread(stream+n);
    if ( ((baitas==0xAF)&&(EEPROMread(stream+n+1)==0xab)) && ( (EEPROMread(stream+n+2)==0xaf)&&(EEPROMread(stream+n+3)==0xab))) {
      int len;
      len=strlen(data);
      data[len+1]=0xAF;
      data[len+2]=0xAB;
      data[len+3]=0xAF;
      data[len+4]=0xAB;
      data[len+5]=0x00;
      return;
    }
    else if (n>492){ //Nesekmes ateju...
      return;
    }
    else{
      data[n]=EEPROMread(stream+n);
      n++;
    }
  }
}

char affs_write(int stream, char *data){
  int dlen=0;
  char *ptr;
  char x;
  int y, z, flenght_old=0;
  int eaddr=511;
  char buff[400], ebuff[4], aeof[]="\xaf\xab\xaf\xab";
  FILEA failai[10];

  //for (y=0; y<400;  

  ptr=&data[0]; 
  //lcd_cls();//!!!
  //cprints("dbg:");//!!!
  //putchar('1');
  while((strcmp(ptr, aeof))!=0){
   ptr++;
   dlen++;
   if (dlen>=490)
    return 0;
  }
  dlen+=4;
  
  //Gaunu dabartini failo ilgi
  ebuff[4]=0;
  //putchar ('2');

  do{
    EEPROMReadBytes(stream+flenght_old, ebuff,4);
    flenght_old++;
  }
  while((strcmp(ebuff, aeof))!=0);
  //putchar ('3');
  flenght_old+=3; //Chk bytes
  //Ilgis gautas.

  affs_list_files(failai); //Gauti failu sarasa
 
  for (x=0; x<=10; x++){
    if (x==11) break;
    else if ((failai[x].address<=(stream+dlen))&&failai[x].address>stream){
      break; //Jei rado faila, kuris butu perrasytas, tai...
    }
  }
  //putchar ('4');
  if (x!=11){ //Jei rado
   z=failai[x].address; //Pirmojo (esancio uz perrasomo failo) failo adresas
   while (eaddr>stream){
    EEPROMReadBytes(eaddr, ebuff, 4);
    if (memcmp(ebuff, aeof, 4)){
      break;
    }
    eaddr--;
   }
   eaddr+=4;      //Gaunu keliamo bloko pabaigos adresa.  (z - pradzia)
   EEPROMReadBytes (z, buff, eaddr-z); //Nuskaitau keliamus duomenis i buferi.
   //putchar ('5');
   for (y=dlen+stream; y<511; y++){
     EEPROMwrite(y, 0); //Seni duomenys trinami, nes yra bufferyje
   }
   EEPROMWriteBytes(dlen+stream, buff, sizeof(buff)); //Keliami duomenys...
   //putchar('6');
   //Toliau tvarkausi su failu isdestymo lentele
   x=0;
   while (failai[x].address>stream)
     failai[x].address+=dlen-flenght_old; //!!!Kas bus jeigu reiksme neigiama?!
  }
  //putchar ('7');
  EEPROMWriteBytes(stream, data, dlen); //Rasomi duomenys i faila...
//Uzkomentinta buvo
/*  else if ((data[dlen-3]==0xaf)&&(data[dlen-2]==0xab)) {
    if ((data[dlen-1]==0xaf)&&(data[dlen]==0xab)){
      EEPROMWriteBytes(stream, data, dlen);
      return 1;
    }
  }

  else if (dlen<=485){
      data[dlen+1]=0xAF;
      data[dlen+2]=0xAB;
      data[dlen+3]=0xAF;
      data[dlen+4]=0xAB;
      data[dlen+5]=0x00;
      EEPROMWriteBytes(stream, data, dlen);
      return 1;
  }  */
  //putchar ('8');
  return 1;
}

int memicmp(char *first, char *second, int n){
 int c=0;
 char f; //, s;
 while (c<n){
  f=first[c];
  n=second[c];
  if (f>='a') f-='a'-'A'; //!!!
  if (n>='a') n-='a'-'A';
  if (f!=n) return n-f;
  c++;
 }
 return 0;
}
