#ifndef __BOOLEAN_H
 #define __BOOLEAN_H
 
 #define TRUE   1
 #define FALSE  0
 typedef unsigned char BOOL;
 
#endif