#ifndef _AFFS_H
#define _AFFS_H 1

#include <eeprom.h>

#define AEOF "\xaf\xab\xaf\xab"
struct file {char name[9];
             unsigned short int address;};
			 
typedef struct file FILEA;

char check_affs (void);
void affs_list_files(FILEA *list);
void affs_format(void);
char affs_new_file (char *name);
int affs_openfile(char *aname);
char *cfname (char *name);
int affs_usedspace(void);
int affs_findfree(void);
int affs_freespace(void);
char affs_write(int stream, char *data);
void affs_read (int stream, char *data);

int memicmp(char *first, char *second, int n);

#endif