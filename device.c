/* 

	Original file name: device.c
	Name			  : Communication with EEPROM. For SFS driver.
	Version			  : v1.0
	Date			  : 2010-09-05
	Compiler		  : ImageCraft ICCAVR
	
	Description: Sios funkcijos skirtos, kad SFS failu sistemos drai-
	veris galetu bendrauti su EEPROM'u. Dabar naudojamas  vidinis  uC
	EEPROM'as, bet veliau planuoju naudoti isorini, didesni.

			  (c)Copyleft 2010, Azuolas - Faustas BAGDONAS

*/

#include "device.h" 

BOOL read_device(char *buffer, unsigned int bytes, unsigned long int address){
  EEPROMReadBytes(address, buffer, bytes);
  return TRUE;
}

BOOL write_device(char *buffer, unsigned int bytes, unsigned int address){
  EEPROMWriteBytes(address, buffer, bytes);
  return TRUE;
}

char read_byte(unsigned int address){
  return EEPROMread(address);
}

BOOL write_byte(unsigned int address, char byte){
  return (EEPROMwrite(address, byte)) ? FALSE : TRUE;
}

unsigned long int get_device_size(void){
  return 1024;
}

