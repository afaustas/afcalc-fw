#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <mem.h>

int menu (void);
//================================
#define AEOF "\xaf\xab\xaf\xab"
struct file {char name[8];
             unsigned short int address;};
			 
typedef struct file FILEA;

char check_affs (void);
void affs_list_files(FILEA *list);
void affs_format(void);
char affs_new_file (char *name);
int affs_openfile(char *aname);
char *cfname (char *name);
int affs_usedspace(void);
int affs_findfree(void);
int affs_freespace(void);
char affs_write(int stream, char *data);
void affs_read (int stream, char *data);
//================================


int EEPROMwrite( int location, unsigned char data);
unsigned char EEPROMread( int addr);
void EEPROMReadBytes(int addr, char *ptr, int size);
void EEPROMWriteBytes(int addr, char *ptr, int size);

char eeprom[513];

void main (void){
  char filename[40];
  FILE *fstream;
  int veiksmas;

  printf ("Ivesti AFFS image varda: ");
  gets (filename);
  if ((fstream = fopen (filename, "r+b"))==NULL){
   printf ("Failas %s nerastas!", filename);
   getch();
   return;
  }
  fread (eeprom, 1, 512, fstream);
  printf ("Failas %s atidarytas!", fstream);

//------------------------------------------------
MMENU:
  while (1){
    veiksmas=menu();

    if (veiksmas==1){  //FORMAT
      char sure='N';
      while (((sure!='y')||(sure!='Y'))||((sure!='n')||(sure!='N'))){
        printf ("Ar tikrai (Y/N)?");
        sure=getch();
        if ((sure=='Y')||(sure=='y')){
          affs_format();
          printf ("\n\nCompleted !");
          getch();
          goto MMENU;
        }
        else if ((sure=='n')||(sure=='N')){
          goto MMENU;
        }
      }
    }

    else if (veiksmas==2){  //Check
      char busena;
      busena=check_affs();
      if (busena==1)
        printf ("\nAFFS OK!\n");
      else
        printf ("\nAFFS FAILED!\n");
      getch();
      goto MMENU;
    }

    else if (veiksmas==3){  //New file
      char fname[9];
      char x;
      printf ("\nNaujo failo vardas (AFFS): ");
      for (x=0; x<9; x++) fname[x]=0; //Isvalomas fname buferis.
      fflush (stdin);
      gets (fname);

      if (affs_new_file(fname))
        printf ("\nCompleted!\n");
      else
        printf ("\nFailed!\n");
      getch();

    }

    else if (veiksmas==5){ //List files
      FILEA *info;
      char success, nr=0, f_vardas[9];
      info=calloc (11, sizeof(FILEA));
      if (info==NULL){
        printf ("system error!");
        return;
      }
      affs_list_files(info);

      printf ("\nFailu sarasas: \n\n");
      while (nr<10){
        memcpy(f_vardas, info[nr].name, 8);
        f_vardas[8]='\0';
        printf ("Name: %s, Address: 0x%X\n", f_vardas, info[nr].address);
        nr++;
      }
      getch();
      free (info);
    }
    else if (veiksmas==8){
      FILEA *failas;
      int busena;
      char *duomenys;
      char name [9];
      printf ("\n\nIvesti failo varda: ");
      fflush(stdin);
      gets(name);
      busena=affs_openfile(name);
      if (busena==0){
        printf ("Failas %s nerastas!", name);
        getch();
      }
      else {
        char pasir;
        duomenys=calloc(491,sizeof(char));
        affs_read(busena, duomenys);
        printf ("\n\n%s\n\n", duomenys);
        printf ("Keisti? (Y/N): ");
        pasir=getchar();
        if ((pasir=='y')||(pasir=='Y')){
          int len;
          printf ("\n\n");
          fflush(stdin);
          //scanf ("%s", duomenys);
          gets(duomenys);
          len=strlen(duomenys);
          memcpy(duomenys+len, AEOF,4);
          affs_write(busena, duomenys);
        }
        free(duomenys);
      }
    }

    else if (veiksmas==10){
      printf ("\nUzimta vieta: %d baitu\n", affs_usedspace());
      printf ("Laisva vieta: %d baitu\n", affs_freespace());
      printf ("Viso: %d baitu\n", affs_usedspace()+affs_freespace());
      getch();
    }

    else if (veiksmas==11){
      int laisva;
      laisva=affs_findfree();
      if (laisva){
        printf ("\nLaisva vieta prasideda adresu: 0x%X\n",laisva);
      }
      else
        printf ("\nVisa vieta uzimta!\n");
      getch();
    }

    else if (veiksmas==12){
      rewind(fstream);
      fwrite (eeprom, 1, 512, fstream);
      return;
    }
  }
}

int menu (void){
  int choice=0;

  clrscr ();
  printf ("--------------------[ AFFS menu ]-----------------------\n\n\n");
  printf (" 1. Format AFFS\n");//
  printf (" 2. Check AFFS\n\n");//
  printf (" 3. Create new file in AFFS volume\n");
  printf (" 4. Erase file in AFFS volume\n");
  printf (" 5. List files AFFS\n");//
  printf (" 6. Rename file\n");
  printf (" 7. Defragment AFFS (do after file deletion)\n");
  printf (" 8. Atidaryti faila\n\n");
  printf (" 9. Gauti failo ilgi\n");
  printf ("10. Uzimta vieta AFFS\n");
  printf ("11. Rasti pirmojo laisvo baito adresa jei yra laisvos vietos headeryje.\n");
  printf ("12. Exit!\n\n");
  printf ("Enter choice: ");
  fflush(stdin);
  scanf ("%d", &choice);
  if (choice < 1 || choice > 12){
    printf ("Ivesta neteisingai!");
    getch();
    choice=0;
    return 0;
  }
  return choice;
}

//-------------- AFFS funkcijos ------------------
char check_affs (void){
  char chk[2];
  EEPROMReadBytes(0x0000, chk, 2); //Reiketu suzinoti kaip tikroje funkcijoje?
  if (strcmp(chk, "\xAF\xAB\xAF"))
    return 1;
  return 0;
}

void affs_list_files(FILEA *list){   //FILEA list - masyvas is 10 elementu
  unsigned short int address=0x0003, faddress;
  char buffer[8];
  FILEA read;
  char fnr=0;

  while ((address<=103)&&(fnr<10)){
    EEPROMReadBytes(address, buffer, 8); //Read to temporary.
    address+=8;
    EEPROMReadBytes(address, &faddress, 2);
    address+=2;
    strcpy (list[fnr].name, buffer);
    list[fnr].address=faddress;
/*    while (x<=10){
      if (x==' '){
        x++;
	    continue;
	  }
      read.name[n][x]=buffer[x];
      x++;
    } */
  
//    read.address[n]=faddress;
  
//   n++;
    fnr++;
  }
//  return read;
}

void affs_format(void){
  int addr;
  char write[]="\xAF\xAB\xAF";
  
  //erasing...
  
  for (addr=0; addr<=512; addr++)
    EEPROMwrite( addr, 0x00);
  
  //Creating affs structure...
  
  EEPROMWriteBytes(0x0000, write, 3);
  
}

char affs_new_file (char *name){
  int haddr=3;
  int baddr=103;
  char stt;
  do{
    stt=EEPROMread(haddr);
    haddr+=10;
  }
  while ((haddr<103)&&stt);
   if (haddr > 10)
     haddr-=10;

  if ((haddr==93)&&(EEPROMread(haddr)>0)){        //
    if (EEPROMread(haddr+1)>0)
      return 0;
  }
  //--
  {
//  char scan=0;
//  char bukle=0;
    char buff[6];

    while ((baddr>0)&&(baddr<510)){
      EEPROMReadBytes(baddr, buff, 6);

      if ((baddr==103&&buff[0]==0)&&buff[1]==0){
     //   baddr=103;
        break;
      }

      if (!(memcmp(buff, "\xaf\xab\xaf\xab\x00\x00", 6))){
        baddr+=4;
        break;
      }

      baddr++;

      if (baddr >= 510)
        baddr=0;
    }
  }
  //---
  EEPROMWriteBytes (haddr, cfname(name), 8);
  EEPROMWriteBytes (haddr+8, (char *)&baddr, 2);
  EEPROMWriteBytes (baddr, AEOF, 4);
  return 1;
}

char *cfname (char *name){        //flush buff?
  static char nname[9];
  char cnn=0, cn=0;
  for (; cnn<9; cnn++) nname[cnn]=0;
  cnn=0;
  while(1){
    if (name[cn]=='.'){
      if (cnn<5){
        nname[cnn]=' ';
        cnn++;
      }
      else
        cn++;
      continue;
    }
    else if ((name[cn]!=0)&&(cnn<=8)){
      nname[cnn]=name[cn];
      cnn++; cn++;
      if (cnn>8)
        return &nname[0]; //???!!!
    }
    else{
      cnn++;
      if (cnn<9){
        nname[cnn]=' ';
        cnn++;
      }
      else 
        return &nname[0];
    }
  }
}

int affs_openfile(char *aname){
  char *name;
  int addr;
  char buff[10];
  int stad=3;

  while (1){
    EEPROMReadBytes(stad, buff, 10);
    stad+=10;

    name=cfname(aname);

    if (!(memicmp(name, buff, 7))){  //Jei 0, tai sutampa.
      addr=buff[8]+buff[9]*256;
      return addr;
    }

    else if (stad>103)
      return 0;
  }
}

int affs_usedspace(){
  return 512-affs_freespace();
}

int affs_freespace(){
  int addr=511, free=0;
  while (!(EEPROMread(addr))){
    addr--;
    free++;
  }
  return free;
}

int affs_findfree(){
  int addr=511;
  if ( (!(EEPROMread(101)))&&(!(EEPROMread(102)))){
    while (!(EEPROMread(addr)))
      addr--;
    return addr;
  }
  else
    return 0;
}

void affs_read (int stream, char *data){    //data - 490 baitu masyvas.
  int n=0;
  unsigned char baitas;
  while (1){
    baitas=EEPROMread(stream+n);
    if ( ((baitas==0xAF)&&(EEPROMread(stream+n+1)==0xab)) && ( (EEPROMread(stream+n+2)==0xaf)&&(EEPROMread(stream+n+3)==0xab))) {
      int len;
      len=strlen(data);
      data[len+1]=0xAF;
      data[len+2]=0xAB;
      data[len+3]=0xAF;
      data[len+4]=0xAB;
      data[len+5]=0x00;
      return;
    }
    else if (n>492){ //Nesekmes ateju...
      return;
    }
    else{
      data[n]=EEPROMread(stream+n);
      n++;
    }
  }
}

char affs_write(int stream, char *data){
  int dlen=0;
  char *ptr;
  char x;
  int y, z, flenght_old=0;
  int eaddr=511;
  char buff[400], ebuff[4];
  FILEA failai[10];

  //for (y=0; y<400;  

  ptr=&data[0];
  while((strcmp(ptr, "\xaf\xab\xaf\xab"))!=0){
   ptr++;
   dlen++;
   if (dlen>=490)
    return 0;
  }
  dlen+=4;

  //Gaunu dabartini failo ilgi
  ebuff[4]=0;
  do{
    EEPROMReadBytes(stream+flenght_old, ebuff,4);
    flenght_old++;
  }
  while((strcmp(ebuff, "\xaf\xab\xaf\xab"))!=0);
  flenght_old+=3; //Chk bytes
  //Ilgis gautas.

  affs_list_files(failai); //Gauti failu sarasa

  for (x=0; x<=10; x++){
    if (x==11) break;
    else if ((failai[x].address<=(stream+dlen))&&failai[x].address>stream){
      break; //Jei rado faila, kuris butu perrasytas, tai...
    }
  }

  if (x!=11){ //Jei rado
   z=failai[x].address; //Pirmojo (esancio uz perrasomo failo) failo adresas
   while (eaddr>stream){
    EEPROMReadBytes(eaddr, ebuff, 4);
    if (memcmp(ebuff, AEOF, 4)){
      break;
    }
    eaddr--;
   }
   eaddr+=4;      //Gaunu keliamo bloko pabaigos adresa.  (z - pradzia)
   EEPROMReadBytes (z, buff, eaddr-z); //Nuskaitau keliamus duomenis i buferi.
   for (y=dlen+stream; y<511; y++){
     EEPROMwrite(y, 0); //Seni duomenys trinami, nes yra bufferyje
   }
   EEPROMWriteBytes(dlen+stream, buff, sizeof(buff)); //Keliami duomenys...

   //Toliau tvarkausi su failu isdestymo lentele
   x=0;
   while (failai[x].address>stream)
     failai[x].address+=dlen-flenght_old; //!!!Kas bus jeigu reiksme neigiama?!
  }

  EEPROMWriteBytes(stream, data, dlen); //Rasomi duomenys i faila...

/*else if ((data[dlen-3]==0xaf)&&(data[dlen-2]==0xab)) {
    if ((data[dlen-1]==0xaf)&&(data[dlen]==0xab)){
      EEPROMWriteBytes(stream, data, dlen);
      return 1;
    }
  }

  else if (dlen<=485){
      data[dlen+1]=0xAF;
      data[dlen+2]=0xAB;
      data[dlen+3]=0xAF;
      data[dlen+4]=0xAB;
      data[dlen+5]=0x00;
      EEPROMWriteBytes(stream, data, dlen);
      return 1;
  }  */
  return 1;
}


//--------------- EEPROM funkcijos -----------------

int EEPROMwrite( int location, unsigned char data){
  eeprom[location]=data;
  return 1;
}

unsigned char EEPROMread( int addr){
  return eeprom[addr];
}

void EEPROMReadBytes(int addr, char *ptr, int size){
  char buffer[513];
  int read=0;
  while (read!=size){
    buffer[read]=eeprom[addr+read];
    read++;
  }
  memcpy(ptr, buffer, size);
}

void EEPROMWriteBytes(int addr, char *ptr, int size){
  int write=0;
  while (write!=size){
    eeprom[addr+write]=ptr[write];
    write++;
  }
}
