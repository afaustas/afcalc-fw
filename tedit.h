#ifndef __TD
 #define __TD
 
 #include <macros.h>
 #include <string.h>
 
 #include "menu.h"
 #include "sfs.h"
 #include "lcdlib.h"
 
 void timer (unsigned int c);
 void td_main(void);
 extern void beep (int kiek, int signalas);
 extern char *input_dlg(void);
 extern void alarm_snd(void);
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 
#endif