#include <iom16v.h>
#include <macros.h>
#include <lcdlib.c>

#pragma interrupt_handler key_press:iv_INT0

char kb_buffer=0; 
char rq_code=0; //Button code
char shift=0;

void lcd_fill (void);
void wait_turn_on (void);
void key_press (void); //ISR
char get_code (void);
void timer (unsigned int c);
void beep (int kiek, int signalas);
char input_number (void);
char input_text(void); //Input text. Returns to kb_buffer.
char main_menu(void); 
void lcd_update_menu(char citm);
void rodykle (void);

/*char input_number (void) {
   char code, get;
   int t;
//   for (t=0; t<3000; t++){
while(1){
    code=get_code();
	switch (code) {
	  case 0xEE: get='1'; timer (40000); break;
	  case 0xDE: get='2'; timer (40000); break;
	  case 0xBE: get='3'; timer (40000); break;
	  case 0x7E: get='*'; timer (40000); break;
	  case 0xED: get='4'; timer (40000); break;
	  case 0xDD: get='5'; timer (40000); break;
	  case 0xBD: get='6'; timer (40000); break;
	  case 0x7D: get=0xFD; timer (40000); break;
	  case 0xEB: get='7'; timer (40000); break;
	  case 0xDB: get='8'; timer (40000); break;
	  case 0xBB: get='9'; timer (40000); break;
	  case 0x7B: get='+'; timer (40000); break;
	  case 0xE7: shift=1; return 0; //Keisti!!!
	  case 0xD7: get='0'; timer (40000); break;
	  case 0xB7: get='='; timer (40000); break;
	  case 0x77: get='-'; timer (40000); break;
//	  case 0x00: return 0;
//	  default: lcd_cls(); printf ("System ERROR !!!\n   HALTED!"); CLI(); while(1); break;
    }
	rq_code=code;
	if (rq_code>0)
	  break;
   }  
//	rq_code=code; //neveikia
	kb_buffer=get; 
	return get;
}
*/



void main( void )
{
  int s=200, w;
  char menu_item_selected;
  char rod1[8]={0x00, 0x04, 0x02, 0x1f, 0x02, 0x04, 0x00, 0x00}; //->
  char rod2[8]={0x00, 0x10, 0x08, 0x1c, 0x08, 0x10, 0x00, 0x00}; // >
  lcd_reset();
  load_cchar(rod1, 0); // ->, 0
  load_cchar(rod2, 8); // >, 1
  wait_turn_on();
  DDRC=255;
  CLI();
  //-----------Port init-------------
  PORTA=0x0F;
  DDRA=0xF0;
  DDRD=0x11; //Buzzer init, Letters switch.
  //-----------INT0 init-------------
  MCUCR=0x03;
  GICR=0x40;
  TIMSK = 0x00;
   
// Start of Quick POST code

  while (s) {
    PORTD=0x10; //1
    for (w=0; w<150; w++); 
    PORTD=0x00; //0
    for (w=0; w<150; w++);
    s--;
  }
  lcd_init(2, 0);
  lcd_cls();
  lcd_fill();
// End Of Quick POST code
  printf ("  CALC made by  Azuolas Bagdonas");
  timer (65000);
  timer (65000);
  timer (65000);
  timer (65000);
  lcd_cls();
  printf ("Firmware: v0.1A");
  timer (65000);
  timer (65000);
  lcd_cls();
  PORTD=1; //Letters switch
  goto IT;
  menu_item_selected=main_menu();
  lcd_cls();
  printf ("Main Menu Returned: ");
  putchar(menu_item_selected+48);
  timer (25000);
  if (menu_item_selected=4){
IT:
    SEI();
    lcd_cls();
	lcd_cursor(2);
	printf ("Input Test: ");
	while(1){
	  if (rq_code>0)
	    putchar(kb_buffer);
		kb_buffer=0;
		rq_code=0;
	}
  }
} 

void lcd_fill(void) {
  int put;
  for (put=0; put<128; put++)
    putchar (255);
  lcd_cls();
}

void wait_turn_on(void) {
  DDRA=0x80; //FE
  PORTA=0x71; 
  while (1) {
    if (!(PINA&1)) {
	  break;
    }
  }
}

//----------------------------------------------------------------------
//                     ISR: key_press
//----------------------------------------------------------------------
void key_press (void) {
char shift=0, code, get;
CLI();
//PORTA=0xAD;
//timer (65000);
//goto end_isr;

  if (!(PIND&1)) {//Is Letters = 1? 
    input_text();
//	if (kb_buffer>0)
//	  putchar(kb_buffer);  //Dar pagalvoti!!!
    PORTA=0x0F;
	SEI();
	return;
	}
  else {
    if (shift==1){
      PORTC=240;
    }
    if (shift>=0) { 
      input_number(); 
    if (kb_buffer>0) 
      putchar(kb_buffer); //Dar pagalvoti!!!
    }
  }
PORTA=0x0F;
SEI();
}
//----------------------------------------------------------------------

char get_code (void) {
  PORTA=0x0F;
  PORTA=127;
  timer (10);
//-----------------
  if (PINA==0X7E) {
//    while (PINA==126) 
    beep (10, 100);
    return 0x7E;
  }
  if (PINA==125) {
//    while (PINA==125)
	  beep (10, 100);
    return 0x7D;
  }
  if (PINA==123){
//    while (PINA==123)
	  beep (10, 100);
   return 0x7B;
  }
  if (PINA==119) {
//    while (PINA==119)
	  beep (10, 100);
    return 0x77;
  }
//-----------------
  PORTA=191;
  timer (10); 
  if (PINA==0xBE) {  
//    while (PINA==0xBE) 
	   beep (10, 100);    
     return 0xBE;
  }
  if (PINA==0xBD) {
//    while (PINA==0xBD) 
	  beep (10, 100);  
     return 0xBD;
  }
  if (PINA==0xBB) {
//    while (PINA==0xBB)
	  beep (10, 100);
    return 0xBB;
  }	 
  if (PINA==0xB7) {
//    while (PINA==0xB7)
	  beep (10, 100);
    return 0xB7;
  }
//-----------------                 
  PORTA=223;
  timer (10);
  if (PINA==0xDE) {      
//    while (PINA==0xDE)
	  beep (10, 100);
    return 0xDE; 
  }
  if (PINA==0xDD) {
//    while (PINA==0xDD)
	  beep (5, 100);   
    return 0xDD;
  }
  if (PINA==0xDB) {
//    while (PINA==0xDB)
	  beep (5, 100);     
    return 0xDB;
  }	
  if (PINA==0xD7) {
//    while (PINA==0xD7)
	  beep (5, 100);  
    return 0xD7;
  }	
//-----------------
  PORTA=239;
  timer (10); 
  if (PINA==0xEE) {    
//    while (PINA==0xEE)
	  beep (5, 100);  
    return 0xEE;  
  }
  if (PINA==0xED) {
//    while (PINA==0xED)
	  beep (5, 100);  
    return 0xED;
  }    
  if (PINA==0xEB) {
//    while (PINA==0xEB)
	  beep (5, 100);   
    return 0xEB;
  }	 
  if (PINA==0xE7) {
//    while (PINA==0xE7)
	  beep (5, 100); 
    return 0xE7;
  }
  return 0;
}

void timer (unsigned int c) {
  int x;
  for (x=0; x<c; x++)
    asm ("NOP");
}

void beep (int kiek, int signalas) { //Nebandyta!!!
  int b, bs;
  for (bs=0; bs<kiek; bs++) {
    for (b=0; b<signalas; b++) {
      PORTD=(PORTD|0x10);
    }
    for (b=0; b<signalas; b++) {
      PORTD=(PORTD&0xEF);
    }
  }
}

char input_number (void) {
   char code, get;
   int t;
//   for (t=0; t<1000; t++){
//    code=get_code();
/*	switch (code) {
	  case 0xEE: get='1'; timer (40000); break;
	  case 0xDE: get='2'; timer (40000); break;
	  case 0xBE: get='3'; timer (40000); break;
	  case 0x7E: get='*'; timer (40000); break;
	  case 0xED: get='4'; timer (40000); break;
	  case 0xDD: get='5'; timer (40000); break;
	  case 0xBD: get='6'; timer (40000); break;
	  case 0x7D: get=0xFD; timer (40000); break;
	  case 0xEB: get='7'; timer (40000); break;
	  case 0xDB: get='8'; timer (40000); break;
	  case 0xBB: get='9'; timer (40000); break;
	  case 0x7B: get='+'; timer (40000); break;
	  case 0xE7: shift=1; return 0; //Keisti!!!
	  case 0xD7: get='0'; timer (40000); break;
	  case 0xB7: get='='; timer (40000); break;
	  case 0x77: get='-'; timer (40000); break;
//	  case 0x00: return 0;
//	  default: lcd_cls(); printf ("System ERROR !!!\n   HALTED!"); CLI(); while(1); break;
    } */
   for (t=0; t < 1000; t++) { //redag!!!
    code=get_code();
	if (code > 0){
	  switch (code) {
	   case 0xEE: get='1'; timer (40000); break;
	   case 0xDE: get='2'; timer (40000); break;
	   case 0xBE: get='3'; timer (40000); break;
	   case 0x7E: get='*'; timer (40000); break;
	   case 0xED: get='4'; timer (40000); break;
	   case 0xDD: get='5'; timer (40000); break;
	   case 0xBD: get='6'; timer (40000); break;
	   case 0x7D: get=0xFD; timer (40000); break;
	   case 0xEB: get='7'; timer (40000); break;
	   case 0xDB: get='8'; timer (40000); break;
	   case 0xBB: get='9'; timer (40000); break;
	   case 0x7B: get='+'; timer (40000); break;
	   case 0xE7: shift=1; return 0; //Keisti!!!
	   case 0xD7: get='0'; timer (40000); break;
	   case 0xB7: get='='; timer (40000); break;
	   case 0x77: get='-'; timer (40000); break;
      } 
	  rq_code=code;
	  code=0;
//  putchar (get); //Debug only!
   }
  }
 kb_buffer=get;
 return get;
}

char input_text (void) {
  unsigned int time;
  char code=0x00, times=0, pressed=0;
  char simb [12][6]={                         //[Pressed][times]
                     'U', 'N', 'U', 'S', 'E', 'D', 
					 '.', ',', '-', '!', '?', ':',
					 'A', 'B', 'C', 0xF7, 0xE4, 0xE8,
					 'D', 'E', 'F', 0xE0, 0xE2, 0xF4,
					 'G', 'H', 'I', '$', '#', '@',
					 'J', 'K', 'L', '%', '^', '&',
					 'M', 'N', '0', '(', ')', '_',
					 'P', 'Q', 'R', 'S', '"', 0x27,
					 'T', 'U', 'V', '<', '>', '=',
					 'W', 'X', 'Y', 'Z', '/', 0x7C,
					 ';', '[', ']', '{', '}', 0xDF,
					 ' ', '0', '+', '-', '*', 0xFD, 
					 };
  
  for (time=0; time < 3000; time++) { //redag!!!
    code=get_code();
	if (code > 0){
	  switch (code) {
	    case 0xEE: times++; pressed=1; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xDE: times++; pressed=2; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xBE: times++; pressed=3; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xED: times++; pressed=4; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xDD: times++; pressed=5; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xBD: times++; pressed=6; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xEB: times++; pressed=7; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xDB: times++; pressed=8; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xBB: times++; pressed=9; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xD7: times++; pressed=11; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break;
	    case 0xE7: times++; pressed=10; putchar (simb[pressed][times-1]); lcd_cursor_back(1); break; //EXTRA
      } 
	  rq_code=code;
	  code=0;
//	  n++;
	  timer (35000); //redag.
	}
  	if (times==6){ //!!!redaguota
	  times=0;
  	  time=0;
  	} 
//  }

//  for (n=0; n<5; n++){
//switch

  }
    if (pressed==0)
	  return 0;
	//lcd_cursor_back (1); 
	//putchar(' ');
	//lcd_cursor_back (1);
	kb_buffer=simb[pressed][times-1];
	return simb[pressed][times-1];
}

char main_menu (void) {
  char item=1;
  lcd_cls();
  printf("   -=[MENU]=-     Calculator");
  SEI();
CHKKEY:
  kb_buffer=0;
  rq_code=0;
  while(1) {
    rodykle();
	if (rq_code>0){
	  if (rq_code==0xB7) //ok
	    return item;
      if (rq_code==0x7b||rq_code==0x77){
	    if (rq_code==0x7b){ //up
		  if (item==1)
		    goto CHKKEY;
		  else {
		    item --;
			lcd_update_menu(item);
			goto CHKKEY;
		  }
		}
		if (rq_code==0x77){ //down
		  if (item==6)
		    goto CHKKEY;
		  else {
		    item++;
			lcd_update_menu(item);
			goto CHKKEY;
		  }
		}
	}
  }
 }
 return 1;
}
  
void rodykle (void) { //Function is used by main_menu() for animated pointer
  lcd_move(18);
  
  putchar (0x00);
  lcd_cursor_back(1);
  timer (25000);
  putchar (0x01);
  lcd_cursor_back(1);
  timer (25000);
}

void lcd_update_menu(char citm){
  lcd_cls();
  lcd_home();
  switch (citm){
    case 1: printf ("   -=[MENU]=-     Calculator  "); break;
	case 2: printf ("  Calculator      Text Editor "); break;
	case 3: printf ("  TextEditor      File Mgr.   "); break;
	case 4: printf ("  File Mgr.       Tools >     "); break;
	case 5: printf ("  Tools >         Memory Dump "); break;
	case 6: printf ("  MemoryDump      About       "); break;
	default: lcd_cls(); printf ("System ERROR !!!\nBad Menu.HALTED!"); CLI(); while(1); break;
  }
}