/* Naudoja mdump.c */
#ifndef __MDUMP_H 
 #define __MDUMP_H 
 
 #include <stdlib.h>
 #include <string.h>
 #include <eeprom.h>
 
 #include "lcdlib.h"
 #include "menu.h"
 
 const char *flash;
 char *ram;
 int x=0, y;
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 
 void mdump_main(void);
 void flashdump(unsigned const char *start, unsigned const char *end);
 void ramdump(unsigned char *start, unsigned char *end);
 void eepromdump(int start, int end);
 extern void timer (unsigned int c);
 
#endif