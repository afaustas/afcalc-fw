head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.33;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@#include "mdump.h"

void mdump_main(void){
  char ret;
  char mdump[5][16]={"[Select  MEMORY]",
                     "  EEPROM",
                     "  SRAM",
					 "  FLASH"};
  lcd_home();
  ret = menu(3, mdump);
  
  switch (ret){
    case 1: 
	  flashdump((const unsigned char *)0x8000, (const unsigned char *)0x8400); 
	  break;
	case 2: 
	  ramdump((unsigned char *)0x0000, (unsigned char *)0x0860);
	  break;
	case 3: 
	  flashdump((const unsigned char *)0x0000, (const unsigned char *)0x8000); 
	  break;
  }
  
}  

/* 

  NOTE: funkcijos  ramdump()  ir  flashdump() yra  visiskai vienodos
  isskyrus tai, kad su flashdump() naudojama CONST. CONST pointerius
  kompiliatorius sukompiliuoja kaip pointerius, kurie  rodo  i flash
  ir EEPROM.
  
  Duomenu isvedimo pavyzdys:

    |0000: ABCDEFGH  |
    |4142434445464748|

*/
  
void flashdump(unsigned const char *start, unsigned const char *end){
   const unsigned char *cur = start;
   unsigned char dump[33], ascii[5];
   unsigned char x;
   
   dump[32] = 0;
   while (cur < end){
	   lcd_cls();

	   /* Address */
	   memset(dump, '0', 32);
	   itoa(ascii, (int) cur, 16);
	   strcpy(&dump[4 - strlen(ascii)], ascii);
	   dump[4] = ':';
	   dump[5] = ' ';
	   
	   /* ASCII */
	   for (x = 0; x < 8; x++){
	       if (*cur)
		     dump[6 + x] = *cur;
		   else
		     dump[6 + x] = '.';
			 
		   if (cur < end)
		     cur++;
	   }

	   /* HEX */
	   dump[14] = ' ';
	   dump[15] = ' ';
	   cur -= 8;
	   for (x = 0; x < 16; x+=2){
		  itoa(ascii, (int)*cur, 16);     
	      memcpy(&dump[16 + x + 2 - strlen(ascii)], ascii, strlen(ascii));
		  if (cur < end)
		    cur++;
	   }

	   prints(dump);
	   kb_buffer = rq_code = 0;
	   while ((kb_buffer != '=') && (rq_code != 0x7B) && (rq_code != 0x77));
	   if(kb_buffer == '=')
	     return;
	   else if (rq_code == 0x7B){
	     if (cur > start) 
		   cur-= 16;
	   }
   }
}

void ramdump(unsigned char *start, unsigned char *end){
   unsigned char *cur = start;
   unsigned char dump[33], ascii[5];
   unsigned char x;
   
   dump[32] = 0;
   while (cur < end){
	   lcd_cls();

	   /* Address */
	   memset(dump, '0', 32);
	   itoa(ascii, (int) cur, 16);
	   strcpy(&dump[4 - strlen(ascii)], ascii);
	   dump[4] = ':';
	   dump[5] = ' ';
	   
	   /* ASCII */
	   for (x = 0; x < 8; x++){
	       if (*cur)
		     dump[6 + x] = *cur;
		   else
		     dump[6 + x] = '.';
		   if (cur < end)
		     cur++;
	   }

	   /* HEX */
	   dump[14] = ' ';
	   dump[15] = ' ';
	   cur -= 8;
	   for (x = 0; x < 16; x+=2){
		  itoa(ascii, (int)*cur, 16);     
	      memcpy(&dump[16 + x + 2 - strlen(ascii)], ascii, strlen(ascii));
		  if (cur < end)
		    cur++;
	   }

	   prints(dump);
	   kb_buffer = rq_code = 0;
	   while ((kb_buffer != '=') && (rq_code != 0x7B) && (rq_code != 0x77));
	   if(kb_buffer == '=')
	     return;
	   else if (rq_code == 0x7B){
	     if (cur > start) 
		   cur-= 16;
	   }
   }
}@
