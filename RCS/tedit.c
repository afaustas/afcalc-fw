head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.34;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@#include "tedit.h"

void td_main(void){
 char ccharset[]= {0x00, 0x00, 0x04, 0x06, 0x1f, 0x06, 0x04, 0x00,
				   0x00, 0x00, 0x08, 0x0c, 0x0e, 0x0c, 0x08, 0x00,
                   0x00, 0x03, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
                   0x00, 0x1f, 0x00, 0x1f, 0x00, 0x1f, 0x00, 0x1f,
				   0x00, 0x10, 0x08, 0x04, 0x04, 0x14, 0x04, 0x14,
				   0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x03, 0x00,
				   0x00, 0x1f, 0x00, 0x1f, 0x00, 0x00, 0x1f, 0x00,
				   0x04, 0x14, 0x04, 0x14, 0x04, 0x04, 0x1f, 0x00};
				   
 /*char sdrv[][16]={"Q: Use:",
                   "  EEPROM[512b]",
		           "  MMC[Nereal.]"}; */
				 
 char new [][16]={"Q: Create new?",
                   "  No",
				   "  Yes"};
				   
 char save[][16]={"Q: Save?",
                   "  Yes",
				   "  No"};
				   
// char ueeprom;
 char buffer [100];
 int n=0, addr;
				
 CLI();   
 lcd_reset();
 load_ccharset(ccharset);
 lcd_init (2,1);
 lcd_home();
 
 putchar(2);
 putchar(3);
 putchar(4);
 lcd_move(16);
 putchar(5);
 putchar(6);
 putchar(7);
 
 lcd_move (4);
 cprints("Text Editor");
 lcd_move (19);
 cprints("2009,AFaustas");
 
 timer (65535);
 timer (65535);
 timer (65535);
 
 /*
 // Comment: Galbut nedarysiu MMC palaikymo. Pereisiu prie ARM arba Intel 8088.
 if (menu (2, sdrv) == 1) ueeprom=1;
 
 else {
   lcd_cls();
   cprints ("Nerealizuota. Halted!");
   while(1);
 }
 */
 
 lcd_cls();
 cprints ("File name: ");
 lcd_move (16);
 cprints ("EEPROM:");
 CLI();
 SEI();
 kb_buffer=0;
 memset(buffer, 0, sizeof (buffer));
 while (n<9){
  if (kb_buffer!=0){
   if (rq_code==0xB7) break;
   buffer[n++]=kb_buffer; //Trynimo galimybes reikia
   putchar (buffer[n-1]);
   kb_buffer=0;
   rq_code=0;
  }
 }
 
 addr=affs_openfile(buffer);
 if (!addr){
   if (menu (3, new)==2){ 
     if (affs_new_file (buffer)){ 
	   addr=affs_openfile(buffer);
	   if (addr==0) {cprints ("FAIL");while(1);}//goto E_FCF;
	 }
	 else {
E_FCF:
	   lcd_cls();
	   cprints ("File creation failed!!!");
	   kb_buffer=0;
	   while (!kb_buffer);
	   return;
	  }
	}
 }
 
 //TXT processor
 buffer[0]=0;
 lcd_cls();
 affs_read (addr, buffer);
 prints (buffer);
 
 kb_buffer=0;
 rq_code=0; //scancode
 n=0;

 while (kb_buffer!='='){
  if (kb_buffer!=0){ 
   buffer[n++]=kb_buffer;
   kb_buffer=0;
   putchar (buffer[n-1]);
  }
 }
 if (menu (3, save)==1){
  int len = strlen (buffer);
  char aeof[] = "\xAF\xab\xaf\xab\0";
  memcpy (buffer+len, aeof, 5);
  affs_write (addr, buffer);
 }
 return;
}
 
void timer (unsigned int c) {
  int x;
  for (x=0; x<c; x++)
    asm ("NOP");
}@
