head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.34;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@#define CALCAPP
#include "calc_app.h"

/* 
  TODO:
  	1. Vartotojo sasaja. Shift - perjungia. (Rodyti "SHIFT")
	2. Memory store/recall from EEPROM (result only)
	3. Error detection
	4. Matemates funkcijos
	5. DEG/RAD (rodyti kol nerodomas rezultatas)
*/

//2. Calculator application.
void calc_main (void){
  InitCalc();
  CalcAppLoop();
}

void InitCalc(void){
  /* Calc LOGO */
  char charset [] = {0x00, 0x07, 0x04, 0x05, 0x05, 0x04, 0x04, 0x05, 
                     0x00, 0x1f, 0x00, 0x1f, 0x1f, 0x00, 0x00, 0x0a,
					 0x00, 0x1c, 0x06, 0x16, 0x16, 0x06, 0x06, 0x16,
					 0x05, 0x04, 0x05, 0x04, 0x05, 0x04, 0x07, 0x03,
					 0x0a, 0x00, 0x0a, 0x00, 0x0a, 0x00, 0x1f, 0x1f,
					 0x16, 0x06, 0x16, 0x06, 0x16, 0x06, 0x1e, 0x1e,
					 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
					 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
					 
  CLI();
  /* Reset & Init. LCD */
  lcd_cls();
  lcd_reset();
  load_ccharset(charset);
  lcd_init(2,1);
  lcd_home();
  //lcd_turn_on(); Not working?? 
  
  /* Print CALC Logo */
  putchar(0x00); 
  putchar(0x01);
  putchar(0x02);
  lcd_move(16);
  putchar(0x03);
  putchar(0x04);
  putchar(0x05);
  lcd_move(3);
  
  /* Jei buvo nuspaustas mygtukas, jo scancode istrinamas is atminties */
  kb_buffer=0;
  
  SEI();
}

void CalcAppLoop (void){
  float result;
  char cb_pos;
  
  /* Main loop */  
  while (1){
    /* Jei buvo nuspaustas mygtukas */
	if (kb_buffer>0){ //Problema cia...???!!!
	  /* Isjungiu INT */
	  CLI();
	  if (kb_buffer=='='){
	    /* '=' Isvedu i LCD */
	    putchar('=');
		/* Ruosiu buferi apdorojimui su skaicavimo algoritmu */
	    calc_buffer[cb_pos] = '=';
		calc_buffer[cb_pos+1] = 0;
		
		/* Atlieku skaiciavimus ir isvedu atsakyma */
		AtliktiSkaiciavimus(calc_buffer, &result, DEG);	//!!!DEG
		print_ats(&result);
		
		/* Laukiu mygtuko paspaudimo... */
		kb_buffer=0;
		SEI();
		while (kb_buffer==0);
		CLI();
		
		/* Isvalau rezultata */
		lcd_move(19);
		cprints ("            ");
		lcd_move(3);
        cprints ("             ");
        lcd_move(3);
		memset(calc_buffer, 0, CB_SIZE); 	
		cb_pos=0;
		kb_buffer=0;
		
		/* Ijungiu INT, tesiu cikla */
		SEI();
		continue;
	  }
	  else if (rq_code==DEL){
		if (cb_pos>0){
		  /* Istrinu simboli is LCD */
		  lcd_putcmd(0x10); //Komanda, istrinanti simboli.
		  putchar (' ');  
		  lcd_putcmd(0x10);
		  
		  /* Istrinu simboli is calc buferio */
		  cb_pos--;
		  calc_buffer[cb_pos]=0x00;
		  rq_code=0;
		  kb_buffer=0;
		  continue;
		}
		
		/* Jei buvo nuspausta DEL ir nebuvo ka trinti, baigti calc_app */
		else 
		  return; 
	  } 
	  
	  /* Jei buvo nuspastas kitas mygtukas */
	  else{
	    /* KOREGUOTI!!!. Pakeisti vartotojo sasaja */
		/* Rasau duomenis is klaviaturos i calc buferi */
	    calc_buffer[cb_pos]=kb_buffer;
	    kb_buffer=0;
	 	cb_pos++;
		
		/* Salyga, jei buferis pilnas */
		if (cb_pos == CB_SIZE){
		  cb_pos=0;
		  lcd_move(19);
		  cprints ("Buffer full!");
		  while (kb_buffer==0);
		  cprints ("             ");
		  lcd_move(3);
		  kb_buffer=0;
		}
	  }
	 
	 lcd_ssl (&calc_buffer[0], 0, 15, 3, FALSE);
	 kb_buffer=0;
	 SEI();
   }//if
 }//while
}

void print_ats(float *ats){
  signed int status;
  char *isvesti, *ptr;
	  
  isvesti=ftoa(*ats, &status);
  lcd_move (19); 
  if ((status==-2)||(status==-1)){
	cprints ("ERROR!");
	return;
  } 
  ptr=(char *)&ats;
  if (ptr[3]&128){ //negative?
	  lcd_move (19);
	  putchar ('-');
  }
  prints (isvesti);
  return;
}

 unsigned char input_math_function(void){
   unsigned char ret;
   rq_code = 0;
   while(!rq_code);
   switch(rq_code){
    ///case 0x??: ret=MATH_F_ID; break;
   }
   
   return ret;
 }

/* ===========================================================================*/

/* Naujo ir patogesnio skaiciavinmo algoritmo realizacija 	   */
/* AtliktiSkaiciavimus() - pagrindine sio alforitmo funkcija   
   char *data - pointeris i stringa su duomenimis, kuriu rezul-
                tata reikia rasti. Stringas su NULL  terminato-
				rium, pries kuri turi buti zenklas '='.
				
				Pvz: "2*3+4="
					 		 	 	  	   		   	   		   */
BOOL AtliktiSkaiciavimus(char *data, float *result, char mode){
	unsigned int x = 0;
	char prioritetai[3][8]={"=()",
							"+-",
							"*/"};
	//float result;

	while (data[x]){
		/* Jei skaicius, dedu ji i skaiciu steka */
		if ((data[x] >= '0') && (data[x] <= '9')) 
			AddSk(data, &x);

		/* Jeigu tai veiksmas, konvertuoju ji i ID (float) ir tada tikrinu jo prioritetus */
		else{
			float steke, cur;

			/* Gaunu is steko skaiciu ir palieku ji ten */
			if (sop)
				steke = sop->data;

			/* Konvertavimas i ID (float) */
			switch(data[x]){
				case '=': cur = LYGU; break;
				case '(': cur = SKLIAUSTAI_ATSIDARO; break;
				case ')': cur = SKLIAUSTAI_UZSIDARO; break;
				case '+': cur = SUDETIS; break;
				case '-': cur = ATIMTIS; break;
				case '*': cur = DAUGYBA; break;
				case '/': cur = DALYBA; break;
				//default: (!!!ERROR!!!)
			}

			/* Zemiausiu prioritetu salyga */
			if (cur <= SKLIAUSTAI_UZSIDARO){
				if(((sop == NULL)||(cur == SKLIAUSTAI_ATSIDARO))&&(cur!=SKLIAUSTAI_UZSIDARO))
					push(&sop, cur);
				else{
					while (sop != NULL){
						Skaiciuoti();
						if (sop){
							steke = sop->data;
							if(sop->data == SKLIAUSTAI_ATSIDARO){
								pop(&sop);
								break;
							}
						}
					}
				}
			}

			/* Vidutiniu prioritetu salyga */
			else if ((cur >= SUDETIS) && (cur <= ATIMTIS)){
				if (sop != NULL){
					/* Steke esancios operacijos prioritetas yra nemazesnis */
					while ((!(steke < SUDETIS))&&(sop != NULL)){
						Skaiciuoti();
						if (sop)
							steke = sop->data;
					}
				}
				push(&sop, cur);				
			}
			
			/* Auksciausiu prioritetu salyga */
			else if ((cur >= DAUGYBA)){
				/* Jei prioritetai lygus */
				while ((steke >= DAUGYBA)&&(sop != NULL)){////
					Skaiciuoti();
					if (sop)
						steke = sop->data;
				}

				/* Jei prioritetas operacijos, esancios steke, yra mazesnis uz dabar nuskaitytaji */
				push (&sop, cur);
			}
		}
		x++;
	}

	*result = pop(&snum);
	return TRUE;
}

/* Ideti skaiciu (argumenta) i skaiciu steka (snum) */
void AddSk(char *data, unsigned int *x){
	unsigned int prad, pab;
	char tmp;
	float skaicius;
	//Leisti: ['0' - '9'], "e+/-", '.'
	pab = prad = *x;

	/* Ieskau skaiciaus pabaigos */
	while (((data[pab] >= '0') && (data[pab] <= '9')) || (data[pab] == '.') || (data[pab] == 'e')){
		/* Exponente? */
		if(data[pab] == 'e'){
			if ((data[pab+1] == '+') || (data[pab+1] == '-'))
				pab++;
		}
		/* toliau ieskoma skaiciaus pabaigos */
		pab++;
	}

	/* Konvertuoju gauta skaiciu i float */
	tmp = data[pab];
	data[pab] = 0;
	skaicius = atof(&data[prad]);
	data[pab] = tmp;

	/* Dedu ji i steka */
	push(&snum, skaicius);

	*x = pab-1; 
}

/* Atlikti steke nurodyta veiksma su virsuje esanciais skaiciais */
void Skaiciuoti(void){
	float a, b, veiksmas, rezultatas;
	b = pop(&snum);
	/* Jei dar steke yra kas nors */
	if (snum)
		a = pop(&snum);
	veiksmas = pop(&sop);

    if (veiksmas == SUDETIS)      rezultatas = a + b;
	else if (veiksmas == ATIMTIS) rezultatas = a - b;
	else if (veiksmas == DAUGYBA) rezultatas = a * b;
	else if (veiksmas == DALYBA)  rezultatas = a / b;
	else if (veiksmas == LYGU)    rezultatas = b;

    push (&snum, rezultatas);
}

/* Darbas su steku, kuri naudoja skaiciavimo algoritmas */
/* push() - raso duomenis i steka 			 			*/
void push (STACK **s, float data){
	STACK *news = malloc(sizeof (STACK));
	news->data = data;
	news->prev = *s;
	*s = news;
}

/* Darbas su steku, kuri naudoja skaiciavimo algoritmas */
/* pop() - isima steko virsuje esanti skaiciu           */
float pop(STACK **s){
	float ret = (*s)->data;
	STACK *prev = (*s)->prev;
	free(*s);
	*s = prev;
	return ret;
}

//----------------------------[END OF CALC_APP.C]-------------------------------
@
