head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.37;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@/* Naudoja mdump.c */
#ifndef __MDUMP_H 
 #define __MDUMP_H 
 
 #include <stdlib.h>
 #include <string.h>
 
 //#include "lcdlib.h"
 #include "menu.h"
 
 const char *flash;
 char *ram;
 int x=0, y;
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 
 void mdump_main(void);
 void flashdump(unsigned const char *start, unsigned const char *end);
 void ramdump(unsigned char *start, unsigned char *end);
 /*void dump(char mem, char ha, int maxmem);
 char dumper_menu(void);
 void drodykle(void);
 void dlcd_update_menu(char citm);*/
 extern void timer (unsigned int c);
 
#endif@
