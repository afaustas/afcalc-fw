head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.33;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@/* Information in file lcdlib.h */
#include "lcdlib.h"

char used=0;

void delst (void) {
  int t;
  for (t=0; t<600; t++); //Value 15000 used ONLY for debug (Default value = 600)
}

void lcd_cls (void) {
  DDRD|=RS_E;
  DDRB=255;
  PORTD&=AND_RS_E; //RS=0 and E=0
  PORTB=CLS;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  used=0;
  delst();
}

void lcd_init (char ln, char cursor) {
  int z;
  for (z=0; z<30000; z++); 
  delst (); 
  DDRD|=RS_E; 
  DDRD|=RW;
  DDRB=255;
  
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=EIGHT_BIT_ONE_LN;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  delst();
  
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=EIGHT_BIT_ONE_LN;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  delst();
  //----------------
  if (ln==2)          //Set 8Bit and n ln mode
    PORTB=EIGHT_BIT_TWO_LN;
  if (ln==1)
    PORTB=EIGHT_BIT_ONE_LN;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  delst();
  //----------------
  if (cursor==1)      //Turn ON display
    PORTB=ON_CURSOR_NO_UNDERLN;
  if (cursor==0)
    PORTB=ON_NOCURSOR_NO_UNDERLN;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  delst();
  //Now you can send a symbol to LCD.
}

void lcd_reset (void) {
  DDRD=DDRD|RS_E;
  DDRC=255;
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=RESET;
  PORTD=PORTD|E;
  delst();
  PORTD=PORTD&AND_E;
  delst();
  PORTD=PORTD&AND_RS_E;
  PORTB=RESET;
  PORTD=PORTD|E;
  delst();
  PORTD=PORTD&AND_E;
  delst();
  delst();
  used=0;
}

void putchar (char c) {
  char f;
  DDRD=DDRD|RS_E;
  DDRC=255;
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=c;
  PORTD=PORTD|RS; //RS=1
  PORTD=PORTD|RS_E; //E=1
  PORTD=PORTD&AND_E; //E=0
  delst();
  PORTD=PORTD&AND_RS; //RS=0
  used++;
  if (used == 16) {
    for (f=0; f<LCD; f++){
	  lcd_send_data(' ');
	}
	used=17;
  }
  if (used == 33) {
    lcd_home ();
	used=0;
  }
}

void prints (char *string) {
  char n;
  for (n=0; string[n]>0; n++)
    putchar (string[n]);
}

void cprints (const char *string) {
  char n;
  for (n=0; string[n]>0; n++)
    putchar (string[n]);
}

void lcd_putcmd (char cmd) {
  DDRD=DDRD|RS_E;
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=cmd;
  PORTD=PORTD|E;
  PORTD=PORTD&AND_E;
  delst();
  }
  
void lcd_send_data (char data) {
  DDRD=DDRD|RS_E;
  DDRC=255;
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  PORTB=data;
  PORTD=PORTD|RS; //RS=1
  PORTD=PORTD|RS_E; //E=1
  PORTD=PORTD&AND_E; //E=0
  delst();
  PORTD=PORTD&AND_RS; //RS=0
}
  
void lcd_home(void) {
  lcd_putcmd(CURSOR_HOME);
  used=0;
  }
  
void lcd_cursor (char type) {
  if (type==0) 
    lcd_putcmd(ON_NOCURSOR_NO_UNDERLN);
  if (type==1)
    lcd_putcmd(ON_NOCURSOR_UNDERLN);
  if (type==2)
    lcd_putcmd(ON_CURSOR_UNDERLN);
}

void lcd_off (void) {
  lcd_putcmd (OFF);
}

void lcd_cursor_back (char back) {
  char i, f;
  for (i=0; i<back; i++){
    lcd_putcmd (CBACK);
    used--;
  if (used == 17) {
    for (f=0; f<LCD; f++)
    lcd_putcmd (CBACK);
	used=16;
  }
 } 
}   

void lcd_cursor_fwd (char fwd) {
  char i, f;
  for (i=0; i<fwd; i++) {
    lcd_putcmd(CFWD);
	used++;
    if (used == 16) {
      for (f=0; f<LCD; f++){
	    lcd_send_data(' ');
	  }
	used=17;
  }
 }
}

void load_ccharset (char *simb) {
  char x;
  lcd_putcmd (CGRAM_LOAD);
  for (x=0; x<63; x++){
    lcd_send_data (simb[x]);
  }
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  lcd_cls();
}

void load_cchar (char *csimb, char adr) {
  char x;
  lcd_putcmd (CGRAM_LOAD+adr);
  for (x=0; x<7; x++) 
    lcd_send_data (csimb[x]);
  PORTD=PORTD&AND_RS_E; //RS=0 and E=0
  lcd_cls();
}

void lcd_move (char x) {
  lcd_putcmd(CURSOR_HOME);
  used=0;
  lcd_cursor_fwd (x);
}

void clcd_hsl (const char *str, unsigned int timeout){
  unsigned int x;
  lcd_putcmd(SLINE);
  while (1){
   for (;*str; str++){
     putchar(*str);  
     for (x=0; x<timeout; x++);
   }
  }
  lcd_putcmd (NORMAL_PRINTING);
}

void lcd_hsl (char *str, unsigned int timeout){
  unsigned int x;
  lcd_putcmd(SLINE);
  while (1){
   for (;*str; str++){
     putchar(*str);  
     for (x=0; x<timeout; x++);
   }
  }
  lcd_putcmd (NORMAL_PRINTING);
}

void lcd_ssl (char *str, unsigned int timeout, char from, char to, BOOLEAN interruptable){
  char x, load=0, offset=0, len;
  unsigned int t;
  
  len=from-to;
  while (str[load]){
    lcd_move (to);
    for (x=0; x<len; x++){
	  if (interrupteda && interruptable)
	    return;
	  if (str[load]!=0)
        putchar (str[load]);
	  else
	    return;
		
	  load++;
    }
  offset++;
  for (t=0; t<timeout; t++);
  load-=(load-offset);
  }
}

void clcd_ssl (const char *str, unsigned int timeout, char from, char to, BOOLEAN interruptable){
  char x, load=0, offset=0, len;
  unsigned int t;
  len=from-to;
  while (str[load]){
    lcd_move (to);
    for (x=0; x<len; x++){
	  if (interrupteda && interruptable)
	    return;
	  if (str[load]!=0)
        putchar (str[load]);
	  else 
	    return;
	  load++;
    }
  offset++;
  for (t=0; t<timeout; t++);
  load-=(load-offset);
  }
}
//===========================[End Of File]==============================
  @
