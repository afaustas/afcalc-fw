head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.37;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@/* Naudoja calc_app.c */
#ifdef CALCAPP
#ifndef __CALC_APP_H
 #define __CALC_APP_H 
 
 #include <math.h>
 #include <macros.h>
 #include <stdlib.h>
 #include <string.h>

 #include "boolean.h"
 //#include "lcdlib.h"

 #define CB_SIZE   	  			    64
 #define DEL 	   	  			    0xE7
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 char calc_buffer[CB_SIZE];
 
 void calc_main (void);
 void print_ats(float *ats);
 void InitCalc(void);
 void CalcAppLoop (void);
 unsigned char input_math_function(void);
 
 /* Naudoja naujas skaiciavimo algoritmas */
 #define DEG						0
 #define RAD						1

 enum veiksmai{LYGU,
 			   SKLIAUSTAI_ATSIDARO,
			   SKLIAUSTAI_UZSIDARO,
			   SUDETIS,
			   ATIMTIS,
			   DAUGYBA,
			   DALYBA};

 struct stack {float data;
	           struct stack *prev;};
 typedef struct stack STACK;

 void push (STACK **s, float data);
 float pop(STACK **s);

 void AddSk(char *data, unsigned int *x);
 void Skaiciuoti(void);
 BOOL AtliktiSkaiciavimus(char *data, float *result, char mode);

 STACK *snum;
 STACK *sop;
 /* ------------------------------------- */ 
#endif
#endif@
