head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.37;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@#ifndef __TD
 #define __TD
 
 #include <macros.h>
 #include <string.h>
 
 #include "menu.h"
 #include "affs.h"
 //#include "lcdlib.h"
 
 void timer (unsigned int c);
 void td_main(void);
 
 extern volatile unsigned char kb_buffer;
 extern volatile unsigned char rq_code;
 
#endif@
