head	1.1;
access;
symbols;
locks
	ICCAVR:1.1; strict;
comment	@ * @;


1.1
date	2010.04.06.23.22.38;	author ICCAVR;	state Exp;
branches;
next	;


desc
@Initial Checkin
@


1.1
log
@Initial revision
@
text
@#ifndef _AFFS_H
#define _AFFS_H 1

#include <eeprom.h>

#define AEOF "\xaf\xab\xaf\xab"
struct file {char name[9];
             unsigned short int address;};
			 
typedef struct file FILEA;

char check_affs (void);
void affs_list_files(FILEA *list);
void affs_format(void);
char affs_new_file (char *name);
int affs_openfile(char *aname);
char *cfname (char *name);
int affs_usedspace(void);
int affs_findfree(void);
int affs_freespace(void);
char affs_write(int stream, char *data);
void affs_read (int stream, char *data);

int memicmp(char *first, char *second, int n);

#endif@
